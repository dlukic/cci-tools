Name: cci-tools
Version: 0.5.6
Release: 1%{?dist}
Summary: Tools for the CERN Cloud Infrastructure team
Source0: %{name}-%{version}.tar.gz
BuildArch: noarch

Group: CERN/Utilities
License: Apache License, Version 2.0
URL: https://openstack.cern.ch/

BuildRequires: python-devel
Requires: python-arrow
Requires: python-cornerstoneclient
Requires: python-ldap
Requires: python-openstackclient
Requires: python-requests
Requires: python-megabus
Requires: pytz
Requires: cern-get-sso-cookie
# From cci7-utils
Requires: python2-zeep
# From openstack repos
Requires: python-click
Requires: python-tenacity
%if 0%{?el5} || 0%{?el6}
Requires: python-argparse
%endif

%description
Set of Python tools to be used by the Cloud Infrastructure Team

%prep
%setup -q


%build
CFLAGS="%{optflags}" %{__python} setup.py build

%install
%{__rm} -rf %{buildroot}
%{__python} setup.py install --skip-build --root %{buildroot}
install -m 755 scripts/cci-dump-record-producer ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-update-ticket ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-update-quota ${RPM_BUILD_ROOT}/usr/bin
install -m 755 scripts/cci-notify-intervention ${RPM_BUILD_ROOT}/usr/bin

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr (-, root, root)
%{_bindir}/cci-*
%{python_sitelib}/*
%doc

%changelog
* Mon Jul 02 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.6-1
- [OS-6823] New script to check if host host any VMs on it

* Wed Jun 13 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.5-1
- [OS-6623] Add historical option to cci-intervention-manager
- [OS-6699] Support list of execution_IDs for intervention manager

* Tue May 29 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.4-2
- Revert nova override endpoint

* Tue May 29 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.4-1
- [OS-6726] Remove 'dryrun' executions from interventions manager listing
- [OS-6663] Make check interventions job resilient against uppercase + .cern.ch
- Fix alarms with uppercase characters stored with lowercase in SNOW

* Fri May 18 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.3-2
- [OS-6687] Make interventions manager fail when no interventions found

* Thu May 17 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.3-1
- [OS-6639] Refactor generic notify users job
- [OS-6600] Create job "Reschedule intervention"

* Tue May 15 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.5.2-1
- Revert "[OS-6476] Calculate VM numbers relying on child DBs for XSLS"

* Tue May 15 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.1-2
- Include cancel intervention UUID to be ignored

* Fri May 11 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.5.1-1
- [OS-6314] Make CLI command to handle job interventions in RD

* Tue May 08 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.17-3
- Fix check_resources for project deletion

* Fri May 04 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.17-2
- [OS-6427] RD job 'Project delete' should delete resources (VMs, images)

* Wed May 02 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.17-1
- Override nova endpoint for admin operations

* Wed May 02 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.16-1
- [OS-6476] Calculate VM numbers relying on child DBs for XSLS

* Fri Apr 27 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.15-1
- [OS-6371] Fix formatting quota update message

* Wed Mar 28 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.14-1
- Hotfix: Fix cookie collisions between parallel jobs

* Tue Mar 20 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.13-1
- [OS-6290] Make comment unicode by default

* Fri Mar 16 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.12-1
- [OS-6065] Improve minor server intervention
- [OS-6069] Inform users when scheduling for wrong date
- [OS-6246] add project name to intervention announcement

* Mon Feb 12 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.11-1
- [OS-5957] Make "Minor server intervention" more resilient to execution against wrong nodes

* Tue Jan 23 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.10-1
- [OS-5889] Set volume snapshot quotas when creating project
- [OS-5886] Stop reassignation of project creation tickets to Cloud

* Mon Jan 22 2018 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.9-1
- [OS-5689] Resolve GNI tickets with svcrdeck user

* Thu Jan 18 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.8-2
- [OS-5811] Move Check disabled compute nodes
- [OS-5755] Add 'behaviour' option to rundeck scheduler

* Thu Jan 11 2018 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.8-1
- [OS-5778] Move Check cloud_* VMs owners
- [OS-5777] Move Send calendar invitation
- [OS-5776] Move Generic install compute node
- [OS-5775] Move Update Foreman settings
- [OS-5771] Move Generic remove compute node
- [OS-5659] Move XSLS availability and metrics job
- [OS-5616] Move Megabus producer job
- [OS-5125] Map cci-tools with rundeck jobs

* Thu Dec 14 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.7-1
- [OS-5667] Delete old files for servicenow v1
- [OS-5485] Adapt to new record producer with more fields

* Tue Dec 05 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.6-2
- [scripts] Fix cci-update-ticket

* Tue Dec 05 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.6-1
- [OS-5555] Fix cci-health-report to meet new flake8 criteria
- [OS-5343] cci-tools showing warning logs
- [OS-5397] cci-rally-cleanup should do normal deletes (and maybe try with force if it doesn't work?)
- [OS-5470] Refresh snow client with 401 in GNI job
- [OS-5520] Don't check quotas before update quota project in creation/update jobs
- [OS-5398] Enable compute node usually fails when there is a machine in error state
- [OS-5484] Allow verifying multiple egroups
- [OS-5073] Make cci-verify-gni-tickets, cci-update-quota and any others use servicenowv2
- [scripts] Move escalate project job to cci-tools
- [scripts] Move health report for Puppet machines job to cci-tools

* Mon Oct 16 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.5-1
- [OS-5330] sendmail not sending emails when no ics attached
- [OS-5347] Fix logging errors power on-off and lock-unlock scripts
- [OS-5325] Scheduled jobs are executed by svcrdeck

* Mon Oct 09 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.4.4-1
- [OS-5195] Remove reset state from servers in rally cleanup
- [rundeckclient] Verify Rundeck cert in RundeckClient to avoid SSLError
- [OS-5114] New ccitools scripts not displaying logs
- [OS-4981] Improve sysadmin procedure when doing minor interventions

* Thu Sep 21 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.3-1
- Fix update-quota until update to snowclientv2

* Tue Sep 19 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.2-1
- [OS-5039] Make dump-project-request generic for all the record producers
- [OS-5039] Remove dependencies from aitools
- [OS-5039] Use own function to check validities of UUIDs
- [OS-5039] Remove windows check in gni-verify-tickets
- [OS-5039] Use password session in script when using CloudClient
- [OS-5039] Include python-zeep, python-click and python-tenacity and remove
    kerberos dependency

* Tue Sep 05 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.4.1-1
- [OS-4847] Refactor snowclient to minimize ticket changes
- [OS-4709] Use svcrdeck service account to do SNOW requests
- [OS-4868] Improve workflow when enabling project via HW resources

* Fri Aug 11 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.11-2
- [OS-4935] Fix exception when no ticket description

* Thu Aug 3 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.11-1
- [OS-4848] Remove unnecessary fields from "Rundeck - Project Deletion"
- [OS-4871] Rundeck picks wrong name when addressing user
- [OS-4868] Rework enable project for HW resources
- [OS-4816] Improve volume deletion for Rally Cleanup job
- [OS-4779] Fix logging problem with rundeck jobs
- [OS-4733] Add Jose's scripts in cci-tools and rundeck
- [snowclient] Fixed bug that generated project_deletion and project_creation tickets with description filled with comment's content
- [cci-assignment-cleanup] Added return code for rundeck job

* Tue Jul 11 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.10-2
- [OS-4680] Regular cleanup of old Rally jobs
- [cloud] Add methods to retrieve servers and volumes from a given user
- [cloud] Add methods to display tables with detailed inforation about servers and volumes from
- [OS-4736] Improve Rundeck script for check of un-assigned GNI tickets when VM doesn't exist anymore
- [cloud] Add get_server to retrieve servers by name
- [common] Include "-o ConnectTimeout=10 -o ServerAliveInterval=20 -o NumberOfPasswordPrompts=0" as default options for ssh_executor a given user

* Mon Jul 10 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.10-1
- [OS-4713] Remove permissions checks in Rundeck code for quota update
- Add scripts for cleanup of role assignments and iterate over projects
- Fix ticket resolution bug on project deletion job

* Fri Jun 23 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.9-2
- Fix 'department' field bug
- [snowclient] Add ticket resolution to cci-delete-project

* Wed Jun 22 2017  Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.9-1
- [snowclient] Add get_ticket_comments and get_openstack_user to servicenow client
- [OS-4124] Edit 'Check un-assigned GNI tickets' so it does not always resolve tickets
- [OS-4644] Revise project creation from SNOW for HW resources

* Mon Jun 12 2017 Luis Pigueiras <luis.pigueiras@cern.ch> - 0.3.8-1
- Add session parameter in cloudclient
- Make all the code flake8 compliant
- [OS-4591] Print backtraes in cci-tools
- [OS-4615] Create Project Deletion extra methods for Horizon
- [OS-4306] Calendar reminders when notifying about interventions affecting virtual machines

* Mon May 15 2017 Ignacio Dominguez Martinez-Casanueva <i.dominguezm@cern.ch> - 0.3.7-1
- Fix issue when RAM quota has -1
- [OS-4596] Use nova list instead of hypervisor APIs
- Add delete project function mapping for SNOW form
- [snowclient] Add create_project_deletion to servicenow client

* Thu Jan 12 2017 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.6-1
- [cloud] Add glance client

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5.1-1
- [xldap] get_primary_account supports primary, service, and secondary accounts

* Tue Dec 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.5-1
- [xldap] Add get_primary_account

* Thu Nov 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.3-1
- [cloudclient] Fix matching condition on get_hypervisor_by_name

* Thu Nov 28 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.2-1
- [cloudclient] Fix condition on get_hypervisor_by_name

* Thu Nov 15 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3.1-1
- [cci-notify-intervention] Expose mailfrom mailcc & mailbcc to user

* Thu Oct 27 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.3-1
- [rudneckclient] Add missing json import
- [cloudclient] Fix cases where searching for an obj on openstack returns all matching elements
- [cci-notify-intervention] Also fix those cases

* Wed Oct 12 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.2-1
- [cloudclient] Changes to make ccitools work with latest aitools openstack auth version

* Thu Oct 6 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1.1-1
- [snowclient] Add email to ticket watchlist

* Mon Aug 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.1-1
- [xldap] Add function to get_starting_with_egroup_members
- [snowclient] Add restriction to only update tickets assigned to groups the user belongs to
- [snowclient] Add new param 'resolver' to resolve_ticket so tickets get resolved/assigned by this user

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-2
- [cci-update-quota] Small update closing comment

* Tue Jul 19 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.50-1
- [cci-update-quota] Update resolving ticket message
- [xldap] Add couple of functions to get users and egroups names
- [snowclient] Add create_project_creation to servicenow client

* Mon Jun 22 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.49-1
- [snowclient] Add create INC
- [cci-create-project] Many changes to support project properties; accounting_group & type

* Mon May 2 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.48-1
- [cci-notify-intervention] Store VM responsible in lowercase to avoid case sensitive duplicates
- [cci-update-quota] Add comment before scalate quota update request

* Fri Apr 14 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.47-1
- [cloud lib] Verify quota update and bug fixes
- [cci-create-project] Remove HEP and enable project from summary table

* Wed Mar 30 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.46-1
- [cci_project_creation] Improve error messages
- [rundeck lib] SSL verify enabled by default
- [rundeck lib] takeover_schedule improvements
- [ldap lib] Fix cases where xldap query may result empty

* Mon Feb 29 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.45-1
- [xldap lib] Fix slowness when using is_existing_egroup function

* Wed Feb 17 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.44-1
- [cci-update-quota] Fix crash when volume_quota empty

* Fri Feb 5 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.43-1
- [servicenow lib] Add Create request and create_quota_update
- [servicenow lib] Code cleanups
- [cci-update-quota] Addapt script to new lib code

* Fri Jan 8 2016 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.42-1
- [rudneck] Change takeover API endpoint (v14 and up)
- [cci-notify-intervention] Address cases where user does not exist
- [cci-update-quota] Get rid of HEP flavors config
- [cci-update-quota] Add users name to closing message
- [cci-update-quota] Get caller from u_caller_id instead of from sys_created_by
- [cci-update-quota] Fix IDs new 7th volume type

* Tue Nov 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.41-1
- [cci-create-project] Move instance argument where makes more sense
- [cci-notify-intervention] Fix empty user list

* Tue Nov 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.40-1
- [rundeck lib] Fix for takeover schedule
- [cci-tools scripts] Add argument 'instance' to all scripts
- [servicenow lib] Fix for those cases when Rundeck updates a ticket and there is no previous assignee
- [servicenow lib] Remove leading and trailing spaces when reading input from record producer

* Tue Nov 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.39-1
- [general lib] Logging handeling improvements
- [cci-update-quota] Fix "Apply project quota request" shows 0 instead of current value bug
- [rundeck lib] Add run execution by job id
- [rundeck lib] Allow custom headers in functions

* Fri Oct 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.38-1
- [servicenow lib] QuotaUpdateRequestRP more error resilient
- [fim lib] Add enable_project function

* Wed Oct 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.37-1
- [common lib] Check return code when ssh executing
- [cci-notify-intervention] Add vmslist variable
- [servicenow lib] Add new RP fields reflecting the new volume types (wig-cp1, wig-cpio1)

* Sun Oct 4 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.36-1
- [common lib] Add optional params to ssh_executor

* Mon Sep 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.35-1
- [servicenow lib] Make servicenow lib compatible with SNOW tables latest changes
- [servicenow lib] Small fixes

* Fri Sep 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.34-1
- [cci-notify-intervention] Fix problem with project IDs and names
- [rundeck lib] Fix typo
- [common lib] Add new function ssh_executor

* Wed Sep 23 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.33-1
- [cci-notify-users] ldap server address from an external variable
- [cci-update-quota] ldap server address from an external variable
- [common lib] cern_sso_get_cookie define cookie path as param

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.32-1
- [cci-notify-intervention] Better exceptions handling
- [cci-notify-intervention] Add mail-content-type option
- [cci-notify-intervention] ldap server address from an external variable

* Tue Sep 22 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.31-1
- [cci-notify-intervention] Set Nova as source of truth for VM's owner/responsible
- [cci-notify-users] Deprecated in favor of cci-notify-intervention
- [python-landbclient] Removed package dependency

* Wed Sep 16 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.30-1
- New improved version of cci-notify-intervention.
- Add python-landbclient dependency

* Wed Sep 9 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.29-1
- Fix cern_get_sso_cookie common function
- cloud library: Add search project by ID
- cci-notify-users: Projects can be referenced using names or IDs
- cci-create-project: Improve output messages

* Tue Sep 1 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.28-1
- Add cern_get_sso_cookie function to cci-tools common library
- Fix typo on cci-create-project

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.27-1
- Add support for volumes types quota updates

* Thu Aug 13 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.26-1
- Add FIMClient Library
- Update cci-create-project to use FIMClient
- Add default/force exec modes to cci-create-project
- Update ServiceNow Library to selfassign ticket before updating it

* Thu Aug 6 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.25-1
- Add takeover_schedule to Rundeck API Client
- Remove automatic health_check non compatible with SSO auth

* Tue Jul 14 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.24-1
- Support latest updates on "Request for shared Cloud Service Project" RP
- Improve servicenow library code
- Add get tickets by hostname to servicenow library
- Adapt scripts to latest library changes

* Mon Jun 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.23-1
- cci-update-quota: Monospace font for quota summary on snow tickets

* Fri Jun 5 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.22-1
- Improve Rundeck API library to support SSO endpoint auth

* Thu May 28 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.21-1
- cci-notify-intervention: Add OTG substitution, sort summary by vmname and show starting hour.
- Add dryrun/perform behaviour to cci-update-quota

* Thu May 07 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.20-1
- cci-notify-intervention: Add option to notify main user of the VM (not only owners).

* Wed May 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.19-1
- Add initial cci-notify-intervention script (NSC source, Hyperv oriented)
- xldap: add get_security_groups_members function.

* Thu Apr 30 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.18-1
- Fix cornerstone return code and message
- Add egroup name validation

* Wed Apr 29 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.17-1
- Fix listing servers in hypervisor when no servers are found
- Add logging to sendmail client
- Fix issue when vm owner was not found

* Fri Apr 17 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.16-1
- Refactor the XldapClient & add unit tests.
- Add new variables (date, hour and duration) to cci-notify-users script.

* Thu Apr 02 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.15-1
- Fix raise exception and add print messages.

* Wed Apr 01 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.14-1
- Improvements to cci-update-quota script.
- New method to set state 'waiting for user' in ServiceNow
- New method in the cloud client to check flavour access.

* Fri Mar 25 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.13-1
- Add cci-update-quota script
- Add cci-notify-users script
- Fix SnowClient functionality interacting with tickets.
- Fix XldapClient get_egroup_email function.
- Add get_hypervisor_by_name to CloudClient

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.12-1
- New functions in CloudClient to get info about nova services

* Fri Mar 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.11-1
- Add generic writing method & additional snow funcitonality

* Fri Mar 06 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.10-1
- (cci-health-report) Add task & user_id column and make them default.
- (cci-health-report) Rename 'Last Update' column to 'Since'
- (cci-health-report) Refactor code

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-2
- Fix typo on snowclient

* Thu Feb 26 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> - 0.0.9-1
- Add resolve ticket and change FE to snowclient
- Cornerstone endpoint is read from config file
- Add export job definitions and projects to rundeck API client

* Wed Feb 11 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.8-1
- (danielfr) Add rundeck api client
- (lfernand) Add subcommands to cci-create-project (from-input/from-snow)

* Fri Feb 06 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.7-1
- Fix egroup checking for project creation

* Thu Feb 05 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.6-1
- Add cci-health-report to monitor vms & volume status

* Thu Feb 05 2015 Daniel Fernandez Rodriguez <danielfr@cern.ch> 0.0.5-1
- Fix cci-project-create to call Cornerstone

* Thu Jan 29 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.4-1
- (lfernand/danielfr) Add cci-create-project script
- (danielfr) Add get_egroup_email to xldap client

* Wed Jan 21 2015 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.3-1
- Add CloudClient class to interoperate with the CERN cloud
- (danielfr) Add validations for servicenow integration
- (danielfr) Add emails utilities

* Wed Dec 17 2014 danielfr <danielfr@cern.ch> - 0.0.2-1
- Add xldap client
- Add method to write work_note in ServiceNow request

* Fri Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-3
- Add RPM changelog

* Fri Dec 08 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-2
- Bump version (Koji fixes)

* Fri Dec 05 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.1-1
- [cci-dump-project-request] Initial version
- [cci-update-ticket] Initial version
- Add 'servicenow' module

* Tue Dec 02 2014 Luis Fernandez Alvarez <luis.fernandez.alvarez@cern.ch> - 0.0.0
- Initial version
