cci-tools
=========

CERN Cloud Infrastructure Tools (aka `cci-tools`) provides a set of Python
scripts and libraries following good software & coding practices, ensuring as
much as possible the reusability of the code.

Release Management
------------------

### Changelog

In general, all the merge requests done in the repository should cover a new
feature, bug fix, etc... triggering a new version of the package.

This implies that every new feature should:

* Add a new entry in `cci-tools.spec`.
* Bump the version in `cci-tools.spec` and `setup.py`.

### Tests

Integration tests can be run with:

```bash
$ cd path/to/cci-tools
# Copy your local files to a rundeck machine
$ watch -n 1 -d "rsync -azP ./ root@cci-rundecknext-00.cern.ch:/tmp/cci-tools-copy"
# Connect to the machine
$ ssh root@cci-rundecknext-00
# Initialize krb token for the svcrdeck user
$ kinit -kt /etc/rundeck/svcrdeck.keytab svcrdeck
$ cd /tmp/cci-tools-copy
# Run the tests
$ PYTHONPATH=src/ py.test tests/integration/test_snowclientv2.py --cov=ccitools.servicenowv2  --cov-report term-missing -x
```

More testing (not only SNOW) will be added in future versions and Gitlab-CI
integration with it too.


Rundeck mapping to ccitools scripts
------------------

The following list shows the current mapping of Rundeck jobs to ccitools scripts:

### Cloud-Operations project

* Hyper-V
  * [Multiple recreate VM](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/569aa68b-6cea-4ee7-9727-b35366f4c53d): TODO

* Hyper-V/subtasks
  * [Check enough quota](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/15aeda1e-cc17-47a5-8484-3dadb93097c2#definition): TODO
  * [Delete VM & recreate](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/61a314ce-5130-4a65-ad73-f0e2bd670458): TODO
  * [Wait4snapshot & create volume](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/4eec0e3d-d684-4065-8b62-e40acf9c4c3c): TODO

* Project Management/Creation
  * [1. Escalate Project Creation request](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/086c95d1-36a9-432f-a75f-179e94ea6ba2): `cci-escalate-project`
  * [2. Project Creation (from snow)](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/d60f2209-a573-4662-960a-0a885ff67c22): `cci-create-project`
  * [3. Enable project](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/399b08a7-a058-4c27-be53-656b16a1d9e8): `cci-enable-project`
  * [Project Creation (from input values)](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/33cd78f5-e48b-4d7c-8675-07acb1b8c6b7): `cci-create-project`

* Project Management/Deletion
  * [Project Deletion (from input values)](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/e6940edd-9f47-4648-ba6e-6e095417b3a2): `cci-delete-project`
  * [Project Deletion (from snow)](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/207957c5-f0ff-486d-95bc-24ce4f40f807): `cci-delete-project`

* Project Management/Quota Update
  * [1. Escalate Quota request](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/6d1539af-bf0b-4f8d-8b26-1b8fa9ec3c15): `cci-update-quota`
  * [2. Apply Quota request](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/76a1cb8c-8ec4-45a1-8a0e-4ed9cc82990e): `cci-update-quota`

* Snow-man duties/Checks
  * [Check un-assigned GNI tickets](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/9b863164-6a69-47d4-b671-9b721d4f6030): `cci-verify-gni-tickets`
  * [Cleanup of old Rally jobs](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/08051448-5a68-453f-af58-b052f296e38e): `cci-rally-cleanup`
  * [Role assignment cleanup](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/c32afec5-255d-41c8-b747-59f12b2aaef6): `cci-assignment-cleanup`
  * [Check cloud_ VMs owners](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/6597eee3-10ed-4833-b483-24efb046e59c): `cci-check-cloud-vm-owners`
  * [Check disable compute nodes](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/81f1bbd9-0852-4505-8dd8-504a76823a85): `cci-check-disabled-nodes`

* Snow-man duties/Reports
  * [Health report for Puppet machines](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/f05d4a5f-b247-4dfa-9025-cf2dfefb5b02): `cci-health-report-puppet`
  * [Health report for virtual machines](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/54323c6b-b512-45bd-b768-4fa3f61880d5): `cci-health-report`
  * [Health report for volumes](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/d2143d60-5ed7-45cd-b6aa-ccf4c96c138c): `cci-health-report`
  * [XSLS availability and metrics](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/ec3b2f55-bc14-44ff-8df2-ae863a09efe3): `cci-xsls-metrics`

* identities
  * [Reset Fernet Keys](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/c516a933-91ed-431c-bcc2-990aa8762d8b): TODO
  * [Rotate Fernet Keys](https://cci-rundeck.cern.ch/project/Cloud-Operations/job/show/2006bb77-f4e0-45c9-89a8-29640701375a): TODO

### Common project

* [Rundeck scheduler](https://cci-rundeck.cern.ch/project/Common/job/show/ee260afb-680e-41a5-97db-cc446bd7f0c4): `cci-rundeck-scheduler`

* compute
  * [Generic remove compute node](https://cci-rundeck.cern.ch/project/Common/job/show/9fa2f478-d5fc-451a-a7ba-ec06ffed5af6): `cci-delete-hosted-vms`
  * [Lock-Unlock VMs of hypervisors](https://cci-rundeck.cern.ch/project/Common/job/show/c2474d02-8a72-4b0b-90f6-6aa9a1f69bd2): `cci-lock-unlock-hv-servers`
  * [Power on-off VMs of hypervisors](https://cci-rundeck.cern.ch/project/Common/job/show/f5cc8d7a-1e12-4bbe-b7c0-dc4608c4c643): `cci-power-on-off-hv-servers`

* installation
  * [Generic install compute node](https://cci-rundeck.cern.ch/project/Common/job/show/e33b799e-7a93-4287-8006-df771da3e795): `cci-wait-puppet-runs`

* interventions
  * [Send calendar invitation](https://cci-rundeck.cern.ch/project/Common/job/show/1da350f6-8b92-4508-995a-bde070cac6d9): `cci-send-calendar-invitation`

* notifications
  * [Megabus producer](https://cci-rundeck.cern.ch/project/Common/job/show/f7cdd569-a73a-46d0-9138-b255f56fe27b): `cci-megabus-producer`

* servicenow
  * [Print project creation request](https://cci-rundeck.cern.ch/project/Common/job/show/9eee6723-a785-4097-966e-bce27f022af3): `cci-dump-record-producer`
  * [Print project deletion request](https://cci-rundeck.cern.ch/project/Common/job/show/1168b73d-6fdd-4137-b4c9-7d90330ee985): `cci-dump-record-producer`
  * [Print quota update request](https://cci-rundeck.cern.ch/project/Common/job/show/1b3053af-a04e-48a0-a919-31dab46a83d5): `cci-dump-record-producer`
  * [Update ticket](https://cci-rundeck.cern.ch/project/Common/job/show/ce52e1a3-636f-4af7-9226-4720af0879ee): `cci-update-ticket`

### HW-Resources

* Project Management
  * [Apply Quota Request](https://cci-rundeck.cern.ch/project/HW-Resources/job/show/ad45c0a5-5a81-4861-a7ee-fbb7d54f122a): `cci-update-quota`
  * [Enable project](https://cci-rundeck.cern.ch/project/HW-Resources/job/show/2e891fec-7b9e-4023-a828-1c1719dc2c4f): `cci-enable-project`
  * [Project Creation (from snow)](https://cci-rundeck.cern.ch/project/HW-Resources/job/show/14d9ba7f-5dbf-47b7-9142-5d9873fef80d): `cci-create-project`

### Internal

* [Auto delete logs](https://cci-rundeck.cern.ch/project/Internal/job/show/1ddef47c-dd97-4b9d-8fc9-ad7ce6542284): TODO
* [Check interventions](https://cci-rundeck.cern.ch/project/Internal/job/show/c8813033-eb5f-472c-a14d-57098126773e): TODO
* [Check scheduled jobs user](https://cci-rundeck.cern.ch/project/Internal/job/show/17e6bcf8-552c-4d6c-be40-3c10ee024c75): TODO
* [Export projects](https://cci-rundeck.cern.ch/project/Internal/job/show/7af1d373-b950-4db0-9abc-74915293a8d9): TODO
* [Export Rundeck Jobs & Projecst](https://cci-rundeck.cern.ch/project/Internal/job/show/97661eec-cbb0-4d8e-8e44-6451ecd678d3): TODO
* [megabus client](https://cci-rundeck.cern.ch/project/Internal/job/show/7eeda0ec-0a61-4a07-82a9-afc91a28c845): TODO
* [Reload nodes](https://cci-rundeck.cern.ch/project/Internal/job/show/20128105-f45c-45b0-83f0-1b86d2d88e10): TODO
* [Takeover scheduled jobs](https://cci-rundeck.cern.ch/project/Internal/job/show/97c6db28-f618-4f81-b9d7-8c7e5763e015): TODO
