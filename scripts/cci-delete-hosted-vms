#!/usr/bin/env python

from ccitools.utils import utils
from ccitools.cloud import CloudClient
import click
import ConfigParser
import logging
import time

# configure logging
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger("delete_hosted_vms")

DELETION_TIMEOUT = 120
DELETION_RETRY_INTERVAL = 10


class bcolors:
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ENDC = '\033[0m'


def _wait_for_deletion(host, cloud):
    """Waits for deletion of VMs residing in the specified host

    :param host: Host that contains VMs to be deleted
    :param cloud: CloudClient object
    """
    timeout = time.time() + DELETION_TIMEOUT
    while time.time() < timeout:
        if cloud.get_servers_by_hypervisor(host):
            logger.info("Waiting for Nova to delete all servers...")
            time.sleep(DELETION_RETRY_INTERVAL)
        else:
            logger.info("All servers deleted")
            return
    error_msg = "There was an error deleting '%s' hosted VMs" % (host)
    raise Exception(error_msg)


@click.command()
@click.pass_context
@click.option("--behaviour", required=True, help="Execution as dryrun/perform")
@click.option("--hosts", required=True, help="List of target hypervisors")
def delete_hosted_vms(ctx, behaviour, hosts):

    cloud = ctx.obj["CLOUD_CLIENT"]

    logger.info("Deleting hosted VMs on '%s'..." % hosts)
    for host in hosts.split():
        try:
            logger.info("Getting list of servers from host '%s'..." % host)
            servers = cloud.get_servers_by_hypervisor(host)
            logger.info("'%s' has %s servers running" % (host, len(servers)))

            if behaviour == 'perform':
                for server in servers:
                    logger.info("Deleting server '%s'..." % server.name)
                    cloud.nova.servers.delete(server.id)
                _wait_for_deletion(host, cloud)
            else:
                logger.info("[DRYRUN] Would've deleted %s VMs running on '%s'"
                            % (len(servers), host))

        except Exception, ex:
            logger.error(("%s%s%s") % (bcolors.BOLD, ex, bcolors.ENDC))

    time.sleep(1)  # log messages sync
    print ""


if __name__ == "__main__":
    # read config file from cci-tools
    config = ConfigParser.SafeConfigParser()
    config.read("/etc/cci/ccitools.cfg")

    # initialize cloud client
    cloud_user = config.get("openstack", "user")
    cloud_pass = config.get("openstack", "pass")
    cloud_auth_url = config.get("openstack", "auth_url")
    cloud_tenant = "admin"  # admin project
    session = utils.get_openstack_session(cloud_user, cloud_pass,
                                          cloud_auth_url, cloud_tenant)
    cloudclient = CloudClient(session=session)

    delete_hosted_vms(obj={
        "CONFIG": config,
        "CLOUD_CLIENT": cloudclient
    })
