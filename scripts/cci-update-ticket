#!/usr/bin/python

import logging
import sys

from argparse import ArgumentParser
from ccitools.common import add_logging_arguments, configure_logging
from ccitools.errors import CciSnowTicketError
from ccitools.servicenowv2 import ServiceNowClient
from ConfigParser import ConfigParser

# configure logging
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(name)s - %(message)s")
logger = logging.getLogger("update_ticket")


def update_main(snowclient, ticket_number, comment, worknote):
    ticket = snowclient.ticket.get_ticket(ticket_number)
    try:
        if comment:
            ticket.add_comment(comment)
            logger.info(
                "'%s' updated with the following comment: '%s'"
                % (ticket_number, comment))
        if worknote:
            ticket.add_work_note(worknote)
            logger.info(
                "'%s' updated with the following worknote: '%s'"
                % (ticket_number, worknote))
    except CciSnowTicketError, msg:
        logger.warning(str(msg))

    ticket.save()


def main():
    parser = ArgumentParser(description="Update a ServiceNow ticket")
    add_logging_arguments(parser)
    parser.add_argument("--ticket-number", required=True)
    parser.add_argument("--instance", default="cern",
                        help="Service now instance")
    parser.add_argument("--comment", dest='comment')
    parser.add_argument("--worknote", dest='worknote')

    args = parser.parse_args()
    if not (args.comment or args.worknote):
        parser.error('Nothing to do. Add --comment or --worknote')

    configure_logging(args)

    config = ConfigParser()
    config.readfp(open('/etc/cci/ccitools.cfg'))

    snowclient = ServiceNowClient(instance=args.instance)
    update_main(snowclient, args.ticket_number, args.comment, args.worknote)


if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        logger.exception(e)
        sys.exit(1)
