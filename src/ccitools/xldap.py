import logging
import re

from ccitools.errors import CciXldapNotFoundError
import ldap
import ldap.sasl

logger = logging.getLogger(__name__)


class XldapClient(object):
    def __init__(self, server, kerberized=False):
        self.server = server
        self.kerberized = kerberized

    def __init_connection(self, kerberized):
        connection = ldap.initialize(self.server)
        if kerberized:
            auth_tokens = ldap.sasl.gssapi()
            connection.sasl_interactive_bind_s('', auth_tokens)
        return connection

    def __search(self, searchFilter, retrieveAttributes,
                 baseDN="DC=cern,DC=ch"):
        connection = self.__init_connection(self.kerberized)
        searchScope = ldap.SCOPE_SUBTREE
        return connection.search_s(baseDN, searchScope,
                                   searchFilter, retrieveAttributes)

    def get_vm_main_user(self, vm_name):
        """Queries xldap.cern.ch to return the main usr of the given VM

        :param vm_name: VM name
        :returns: VM main user
        """
        if vm_name and re.search("(.*).cern.ch", vm_name):
            vm_name = re.search("(.*).cern.ch", vm_name).group(1)
        searchFilter = "(&(objectClass=computer)(cn=%s))" % vm_name
        retrieveAttributes = ['manager']
        result = self.__search(searchFilter, retrieveAttributes)
        try:
            return result[0][1]['manager'][0].split(',')[0].split('=')[1]
        except Exception:
            logger.warning("'%s' main user not found" % (vm_name))
            raise CciXldapNotFoundError(
                "VM '%s' main user not found" % (vm_name))

    def get_vm_owner(self, vm_name):
        """Queries xldap.cern.ch to return the username who manages the given VM

        :param vm_name: VM name
        :returns: VM owner
        """
        if vm_name and re.search("(.*).cern.ch", vm_name):
            vm_name = re.search("(.*).cern.ch", vm_name).group(1)
        searchFilter = "(&(objectClass=computer)(cn=%s))" % vm_name
        retrieveAttributes = ['managedBy']
        result = self.__search(searchFilter, retrieveAttributes)
        try:
            return result[0][1]['managedBy'][0].split(',')[0].split('=')[1]
        except Exception:
            logger.warning("'%s' owner not found" % (vm_name))
            raise CciXldapNotFoundError("VM '%s' owner not found" % (vm_name))

    def get_egroup_email(self, egroup):
        """Queries xldap.cern.ch to return email address of the given egroup

        :param egroup: E-group name
        :returns: E-group email address
        """
        searchFilter = "(&(objectClass=group)(cn=%s))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        retrieveAttributes = ['mail']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        if result:
            if result[0][1]:
                return result[0][1]['mail'][0]
        logger.warning("'%s' egroup not found" % (egroup))
        raise CciXldapNotFoundError("'%s' egroup not found" % (egroup))

    def get_egroup_members(self, egroup):
        """Get egroup members

        Queries xldap.cern.ch to return the recursive list of members of the
        given egroup

        :param egroup: E-group name
        :returns: List of members
        """
        members = []
        searchFilter = "(&(objectClass=group)(cn=%s))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        retrieveAttributes = ['member']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        if result:
            if result[0][1]:
                for member in result[0][1]['member']:
                    membername = member.split(',')[0].split('=')[1]
                    if self.is_existing_egroup(membername):
                        logger.debug(
                            "Found e-group '%s' member of '%s'. "
                            "Getting members..." % (membername, egroup))
                        members += self.get_egroup_members(membername)
                    elif member not in members:
                        members.append(membername)
            return members
        raise CciXldapNotFoundError("'%s' egroup not found" % (egroup))

    def get_primary_account(self, username):
        """Queries xldap.cern.ch to return the main user assigned to the given username

        :param username: User name
        :returns: Username responsible
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['cernAccountType',
                              'cernAccountOwner', 'sAMAccountName']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        if result:
            account_type = result[0][1]['cernAccountType'][0]
            if account_type.lower() == 'primary':
                return result[0][1]['sAMAccountName'][0]
            elif account_type.lower() in ['secondary', 'service']:
                return (
                    result[0][1]['cernAccountOwner'][0].split(',')[0]
                    .split('=')[1]
                )
        raise CciXldapNotFoundError("User '%s' not found" % (username))

    def get_user_email(self, username):
        """Queries xldap.cern.ch to return email address of the given username

        :param username: User name
        :returns: Username's email address
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['mail']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        if result:
            if result[0][1]:
                return result[0][1]['mail'][0]
        logger.warning("User '%s' not found" % (username))
        raise CciXldapNotFoundError("User '%s' not found" % (username))

    def get_email(self, name):
        """Get email from user

        Queries xldap.cern.ch to return email address of the given username or
        egroup

        :param name: Username or egroup to get the email address for
        :returns: Params's email address
        """
        if self.is_existing_user(name):
            return self.get_user_email(name)
        if self.is_existing_egroup(name):
            return self.get_egroup_email(name)
        raise CciXldapNotFoundError("'%s' not found" % (name))

    def get_security_groups_members(self, security_group):
        """Get security groups members

        Queries Active Direcotry to return the list of members of the given
        security group

        :param security_group: security group name
        :returns: List of members
        """
        members = []
        baseDN = "OU=NSC,OU=CMF,DC=cern,DC=ch"
        searchFilter = "(&(objectClass=group)(cn=%s))" % security_group
        retrieveAttributes = ['member']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        if result:
            if result[0][1]:
                for member in result[0][1]['member']:
                    members.append(member.split(',')[0].split('=')[1])
                return members
            raise CciXldapNotFoundError(
                "No members found in '%s'" % (security_group))
        raise CciXldapNotFoundError(
            "'%s' Security Group not found" % (security_group))

    def get_starting_with_egroups(self, egroup):
        searchFilter = "(&(objectClass=group)(cn=%s*))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        result = self.__search(searchFilter, None, baseDN)
        egroup_list = []
        if result and result[0][1]:
            for egroup in result:
                egroup_list.append(egroup[1]['mailNickname'][0])
        return egroup_list

    def get_starting_with_egroup_members(self, egroup, username):
        canonicalName = ("CN=%s,OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
                         % egroup)
        searchFilter = ("(&(objectClass=user)(memberof=%s)(cn=%s*))"
                        % (canonicalName, username))
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['sAMAccountName']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        user_list = []
        if result and result[0][1]:
            for user in result:
                user_list.append(user[1]['sAMAccountName'][0])
        return user_list

    def get_starting_with_users(self, username):
        searchFilter = "(&(objectClass=user)(cn=%s*))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        retrieveAttributes = ['sAMAccountName']
        result = self.__search(searchFilter, retrieveAttributes, baseDN)
        user_list = []
        if result and result[0][1]:
            for user in result:
                user_list.append(user[1]['sAMAccountName'][0])
        return user_list

    def is_existing_user(self, username):
        """Checks on xldap.cern.ch if the given user exists

        :param username: User name
        :returns: bool
        """
        searchFilter = "(&(objectClass=user)(cn=%s))" % username
        baseDN = "OU=Users,OU=Organic Units,DC=cern,DC=ch"
        result = self.__search(searchFilter, None, baseDN)
        if result and result[0][0]:
            return True
        return False

    def is_existing_egroup(self, egroup):
        """Checks on xldap.cern.ch if the given e-group exists

        :param egroup: E-group name
        :returns: bool
        """
        searchFilter = "(&(objectClass=group)(cn=%s))" % egroup
        baseDN = "OU=e-groups,OU=Workgroups,DC=cern,DC=ch"
        result = self.__search(searchFilter, None, baseDN)
        if result and result[0][0]:
            return True
        return False
