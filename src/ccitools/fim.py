#!/usr/bin/env python

from ccitools.utils import utils
from zeep.client import Client


class FIMClient(object):

    def __init__(self, url):
        transport = utils.get_fim_transport(url)
        self.client = Client(url, transport=transport).service

    def create_project(self, name, description, owner, shared=True):
        """Creates a new OpenStack Project using FIM Web Service

        :param name: Project name
        :param description: Project description
        :param owner: Project owner
        :param shared: boolean
        """
        return self.client.CreateProject(name=name, description=description,
                                         owner=owner, shared=shared)

    def delete_project(self, name):
        """Deletes an existing OpenStack Project using FIM Web Service

        :param name: Project name
        """
        return self.client.DeleteProject(name=name)

    def enable_project(self, name):
        """Enables an OpenStack Project using FIM Web Service

        :param name: Project name
        """
        return self.client.SetProjectStatus(name=name, enabled=True)

    def is_valid_owner(self, username):
        """Checks if user is a valid OpenStack Project owner using the FIM Web Service

        :param username: User name
        """
        return self.client.ValidateOwner(username)


class FIMCodes(object):
    SUCCESS = 0
    ERROR_INVALID_PARAMS = 1
    ERROR_NOT_SUSCRIBED = 2
    ERROR_PROJECT_EXISTS = 3
    INTERNAL_ERROR = -1
