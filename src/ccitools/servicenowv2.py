#!/usr/bin/env python

import logging

from ccitools.utils.snow import assignment_group
from ccitools.utils.snow import fe
from ccitools.utils.snow import record_producer
from ccitools.utils.snow import ticket
from ccitools.utils.snow import user
from ccitools.utils import utils

logger = logging.getLogger(__name__)


class ServiceNowClient(object):
    """Wrapper to do Service Now operations over tickets

    It includes the necessary methods to create tickets and perform
    some one step operations that need some other APIs to be performed
    (e.g: adding a user to a watch_list requires querying the
    users table to map the id with the real name or change the FE which
    requires some calculations to see which SE is the one that fits better
    the FE).

    Also, it exposes interfaces to perform operations over the SNOW API,
    depending on which kind of operation we want to do (ticket,
    record_producer, fe or users)

    Usage example:

    >>> snow_cl = ServiceNowClient()
    >>> ticket = snow_cl.ticket.create_INC(short_description="...",
                                           functional_element="...",
                                           assignment_group="...")
    >>> ticket.add_comment("My comment")
    >>> snow_cl.change_functional_element(ticket, "my fe", "my assignment grp")
    >>> ticket.add_work_note("My work note")
    >>> ... # do other operations
    >>> ticket.save()  # remember to save after doing changes on the ticket

    Usage example for a record producer:

    >>> ticket = snow_cl.ticket.create_RQF(...)
    >>> data = {...}
    >>> record_producer = snow_cl.record_producer.convert_RQF_to_quota_update(ticket, data)  # noqa
    >>> record_producer.cores  # To get information from the RP
    >>> record_producer.request.add_comment("...")  # Normal ticket ops
    >>> record_producer.request.save()
    """

    def __init__(self, instance="cern"):
        """Init method for the client of ServiceNow

        :param instance: the instance of service now to use. It can be
            `cerntest`, `cerndev` or `cern` if we want to use the production
            instance. (default: `cern`)
        """
        # Initialize the requests session to query CERN service now
        # Relies on Kerberos to authenticate with the svcrdeck user
        transport = utils.get_snow_transport(instance)

        # Initialize SOAP clients to perform different operations on
        # Service Now tables
        self.ticket = ticket.TicketSoap(instance, transport)
        self.record_producer = record_producer.RecordProducerSoap(
            instance, transport)
        self.assignment_group = assignment_group.AssignmentGroupSoap(
            instance, transport)
        self.fe = fe.FunctionalElementSoap(
            instance, transport)
        self.user = user.UsersSoap(instance, transport)

    def add_user_watch_list(self, ticket, user_name):
        """Add user to the watch_list

        :param ticket: a `Ticket` instance
        :param user_name: the user we want to add in the watch list of the
            ticket
        """
        logger.info("Adding {user} to watch list".format(user=user_name))

        user = self.user.get_user_info_by_user_name(user_name)
        ticket.add_user_id_to_watch_list(user.sys_id)

    def change_functional_element(self, ticket, fe_name,
                                  assignment_group_name):
        """Change FE in a ticket

        :param ticket: the ticket that needs to change the FE
        :param fe_name: name of the new FE
        :param assignment_group_name: name of the new assignment group
        """
        fe = self.fe.get_fe_by_name(fe_name)
        se = self.fe.get_best_se(fe.sys_id)

        logger.info("Changing ticket FE to '{fe}' with '{assignment_group}'"
                    .format(fe=fe_name,
                            assignment_group=assignment_group_name))

        ticket.change_fe(fe.sys_id, se.sys_id, assignment_group_name)

    def get_quota_update_rp(self, ticket_number):
        """Get quota update record producer

        :param ticket_number: a ticket number
        :returns: a `QuotaUpdateRecordProducer` instance
        """
        request = self.ticket.get_ticket(ticket_number)
        return self.record_producer.get_quota_update_rp(request)

    def get_project_creation_rp(self, ticket_number):
        """Get project creation record producer

        :param request: a `Request` instance
        :param ticket_number: a ticket number
        :returns: a `CreateProjectRecordProducer` instance
        """
        request = self.ticket.get_ticket(ticket_number)
        return self.record_producer.get_project_creation_rp(request)

    def get_project_deletion_rp(self, ticket_number):
        """Get project deletion record producer

        :param ticket_number: a ticket number
        :returns: a `DeleteProjectRecordProducer` instance
        """
        request = self.ticket.get_ticket(ticket_number)
        return self.record_producer.get_project_deletion_rp(request)
