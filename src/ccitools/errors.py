#!/usr/bin/env python


class CciCloudNotFoundError(Exception):
    pass


class CciXldapNotFoundError(Exception):
    pass


class CciSnowTicketError(Exception):
    pass


class CciSnowTicketStateError(Exception):
    pass


class CciSnowRecordNotFoundError(Exception):
    pass


class RundeckAPIError(Exception):
    pass
