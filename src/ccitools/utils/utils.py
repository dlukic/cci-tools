from ccitools.common import cern_get_sso_cookie
from keystoneclient.auth.identity import v3
from keystoneauth1 import session as keystone_session
import prettytable
import re
import requests
from zeep.client import Client
from zeep.transports import Transport


def merge_dicts(x, y):
    """Merge two dicts into a new dict as a shallow copy"""
    z = x.copy()
    z.update(y)
    return z


def get_soap_client(instance, table, transport):
    """Get SOAP client for a specific table in a CERN service now

    :param instance: the CERN instance of service now (e.g: `cern` or
        `cerntest`)
    :returns: a zeep client to perform operations over the service now table
    """
    url = "https://%s.service-now.com/%s.do?WSDL" % (instance, table)

    return Client(url, transport=transport).service


def _get_transport(url):
    """Get a transport for the url SSO validated

    :param url: url to get a valid zeep transport for
    :returns: a zeep transport to be used to do operations over service now
    """
    session = requests.Session()
    # It will require a valid KRB token
    session.cookies = cern_get_sso_cookie(url)
    return Transport(session=session)


def get_fim_transport(fim_url):
    """Get a transport session for querying FIM

    :returns: a zeep transport to be used to do operations over FIM
    """
    return _get_transport(fim_url)


def get_resources_info_table(resources, fields):
    """Generates a printable table with
    detailed information about the given resources

    Server example => resources = list_of_server_objects
                      fields = {
                        'ID': 'id',
                        'Name': 'name',
                        'Created': 'created',
                        'Status': 'status',
                        'Project ID': 'tenant_id',
                        'Host ID': 'hostId',
                      }

    Volume example => resources = list_of_volume_objects
                      fields = {
                        'ID': 'id',
                        'Name': 'name',
                        'Created': 'created_at',
                        'Status': 'status',
                        'Project ID': 'os-vol-tenant-attr:tenant_id',
                        'Host ID': 'os-vol-host-attr:host',
                      }

    :param resources: List of resources (MUST BE SAME TYPE e.g. 'Server')
    :param fields: List of tuples that contains information fields to display,
    where each key is the name of a column and each value is its corresponding
    attribute name for the resource (as declared by OpenStack API)
    :returns: Table with detailed information
    """
    row_list = []
    columns = []
    for resource in resources:
        row_values = []
        for column, field in fields:
            try:
                row_values.append(getattr(resource, field))
                if column not in columns:
                    columns.append(column)
            except Exception:
                # In case of non-existing attribute ignore its column
                continue
        row_list.append(row_values)

    table = prettytable.PrettyTable(columns)
    for r in row_list:
        table.add_row(r)

    table.border = True
    table.header = True
    table.align = 'l'
    return table


def get_snow_transport(instance):
    """Get a transport session for querying service now with the same cookie

    :param instance: the CERN instance of service now (e.g: `cern` or
        `cerntest`)
    :returns: a zeep transport to be used to do operations over service now
    """
    snow_url = 'https://%s.service-now.com/' % instance
    # It will require a valid KRB token
    return _get_transport(snow_url)


def get_openstack_session(user, password, auth_url, tenant):
    auth = v3.Password(
        username=user,
        password=password,
        auth_url=auth_url,
        project_name=tenant,
        user_domain_name="default",
        project_domain_name="default",
    )
    return keystone_session.Session(auth=auth)


def normalize_hostname(hostname):
    """Normalizes hostname for checks by setting to lower cases
    and removing cern domain name

    :param hostname: Name of the host
    :returns: Normalized hostname
    """
    # Lower-case the name
    hostname = hostname.lower()

    # Strip cern domain for cleanliness
    if re.search("(.*).cern.ch", hostname):
        hostname = hostname.strip('.cern.ch')

    return hostname


def truncate(string, width=20):
    """Add dots to string if is longer than with

    :param width: maximum width of the string
    :returns: a shortened string
    """
    if len(string) > width:
        string = string[:width-3] + '...'
    return string
