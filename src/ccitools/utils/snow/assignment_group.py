from ccitools.utils import utils


class AssignmentGroupSoap(object):
    ASSIGNMENT_GROUP_TABLE = "sys_user_group"

    def __init__(self, instance, transport):
        self.client = utils.get_soap_client(
            instance, AssignmentGroupSoap.ASSIGNMENT_GROUP_TABLE, transport)

    def get_assignment_group_by_name(self, assignment_group_name):
        """Get assignment group information by name

        :param assignment_group_name: name of the assignment group from which
            we extract the available information
        :returns: a zeep object with the response from the
            assignment group table for that specific group name
        """
        return self.client.getRecords(name=assignment_group_name)[0]
