from ccitools.utils import utils


class UsersSoap(object):
    USERS_TABLE = "sys_user_list"

    def __init__(self, instance, transport):
        self.client = utils.get_soap_client(
            instance, UsersSoap.USERS_TABLE, transport)

    def get_user_info_by_user_name(self, user_name):
        """Get user information

        :param user_name: a user name from which we extract the available
            information
        :returns: a zeep object with the response from the users table for
            that specific name
        """
        return self.client.getRecords(user_name=user_name)[0]

    def get_user_info_by_sys_id(self, sys_id):
        """Get name for user sys id

        :param sys_id: a service now sys id for a user
        :returns: the username that matches that sys_id
        """
        return self.client.getRecords(sys_id=sys_id)[0]
