import abc
from ccitools.utils import utils


class RecordProducerSoap(object):
    """Wrapper to get/update record producers

    To get a list of the DECORATORS and ELEMENT_IDs and what they represent
    you would need to:

    - Create the record producer via the SNOW form
    - Get the sys_id of the ticket
    - Get the list of records associated with the sys_id of the ticket
    - Then iterate over them. If they have a value, associate
        the `question` field of the record with a `label`. Include the
        order too.
    - If they don't have a value they should be included as a decorator.
    """

    RECORD_PRODUCER_ID = "dfb4225078a27100de14a0934907b597"
    RECORD_PRODUCERS_TABLE = "question_answer_list"

    def __init__(self, instance, transport):
        self.client = utils.get_soap_client(
            instance, RecordProducerSoap.RECORD_PRODUCERS_TABLE, transport)

    def get_quota_update_rp(self, request):
        """Get quota update record producer

        :param request: a `Request` instance
        :returns: a `QuotaUpdateRecordProducer` instance
        """
        return QuotaUpdateRecordProducer(
            request, self._get_record_field_values(request))

    def get_project_creation_rp(self, request):
        """Get project creation record producer

        :param request: a `Request` instance
        :returns: a `CreateProjectRecordProducer` instance
        """
        return CreateProjectRecordProducer(
            request, self._get_record_field_values(request))

    def get_project_deletion_rp(self, request):
        """Get project deletion record producer

        :param request: a `Request` instance
        :returns: a `DeleteProjectRecordProducer` instance
        """
        return DeleteProjectRecordProducer(
            request, self._get_record_field_values(request))

    def _convert_RQF(self, request, data, record_project_class):
        """Convert RQF into a specific record producer filling the custom form

        Convert RQF in a record producer using:

        - record_project_class.LABEL_TO_ELEMENT_ID: indicate to which snow
            sys id correlates with which data key
        - record_project_class.LABEL_TO_ORDER: they are the map
            between a sys_id and where
        - record_project_class.DECORATORS: they are the fields that don't
            include any information in the form, that are used to make
            the visualization in the ServiceNow webpage better. First field
            of the tuple is a `sys_id` and second one is the `order`

        :param request: an existing `Request` instance
        :param data: a dict with the necessary fields that need to be fill
            in the record producer form
        :param record_project_class: a `class` that should have
            `LABEL_TO_ELEMENT_ID`, `DECORATORS` and `LABEL_TO_ORDER`
        :returns: a `RecordProducer` instance
        """
        # fill the form with the data provided in data
        for key, value in data.iteritems():
            try:
                element_id = record_project_class.LABEL_TO_ELEMENT_ID[key]
                self.client.insert(
                    table_sys_id=request.info.sys_id,
                    table_name=request.info.sys_class_name,
                    question=element_id,
                    value=value,
                    # includes the order to specify where the fields should
                    # appear in the webpage
                    order=record_project_class.LABEL_TO_ORDER[key],
                )

            except KeyError:
                pass  # ignore the keys that are not included in the form

        # complete the record producer information inserting
        # extra decoration in the form
        for element_id, order in record_project_class.DECORATORS:
            self.client.insert(
                table_sys_id=request.info.sys_id,
                table_name=request.info.sys_class_name,
                question=element_id,
                value=None,
                order=order
            )

        # welp, this is a bit hackish
        try:
            description = data["description"]
        except KeyError:
            description = data["comment"]

        # we convert the ticket into a record producer, this means that
        # it will use the information we inserted in the record producer table
        # and will fill the form accordingly with these values
        request.update(u_caller_id=data["username"],
                       description=description,
                       u_record_producer=RecordProducerSoap.RECORD_PRODUCER_ID)

        comment = u"""{0}

Cloud Service Automation (on behalf of the user)""".format(data["comment"])

        request.add_comment(comment)

        return record_project_class(
            request, self._get_record_field_values(request))

    def convert_RQF_to_quota_update(self, request, data):
        """Convert the request to a quota update record producer

        Convert the request to a quota update record producer. All the
        necessary fields are:

            data = {
                "username": "my_username",
                "project_name": "Cloud Monitoring",
                "comment": "Hello, testing",
                "instances": "new_number_of_instances",
                "cores": "new_number_of_cores",
                "ram": "new_ram",
                "cp1_gigabytes": "new_gb_cp1",
                "cp1_volumes": "new_volumes_cp1",
                "cpio1_gigabytes": "new_gb_cpio1",
                "cpio1_volumes": "new_volumes_cpio1",
                "io1_gigabytes": "new_gb_io1",
                "io1_volumes": "new_volumes_io1",
                "standard_gigabytes": "new_gb_standard",
                "standard_volumes": "new_volumes_standard",
                "wig_cp1_volumes": "new_volumes_wig_cp1",
                "wig_cp1_gigabytes": "new_gb_wig_cp1",
                "wig_cpio1_volumes": "new_volumes_wig_cpio1",
                "wig_cpio1_gigabytes": "new_gb_wig_cpio1",
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        # this fills the volume type dropdowns with the correct volume
        # types
        data["standard"] = "standard"
        data["cp1"] = "cp1"
        data["cpio1"] = "cpio1"
        data["io1"] = "io1"
        data["wig_cp1"] = "wig_cp1"
        data["wig_cpio1"] = "wig_cpio1"
        data["visible_cp1"] = "Yes"
        data["visible_cpio1"] = "Yes"
        data["visible_io1"] = "Yes"
        data["visible_wig_cp1"] = "Yes"
        data["visible_wig_cpio1"] = "Yes"

        return self._convert_RQF(request, data, QuotaUpdateRecordProducer)

    def convert_RQF_to_project_creation(self, request, data):
        """Convert the request to a project creation record producer

        Convert the request to a project creation record producer. All the
        necessary fields are:

            data = {
                "accounting_group": "ALICE",
                "comment": "Hello, testing",
                "cores": 5,
                "description": "my description",
                "egroup": "cloud-infrastructure-3rd-level",
                "gigabytes": 5,
                "instances": 5,
                "owner": "lpigueir",
                "project_name": "Openstack test",
                "ram": 10,
                "username": "lpigueir",
                "volumes": 5,
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        return self._convert_RQF(request, data, CreateProjectRecordProducer)

    def convert_RQF_to_project_deletion(self, request, data):
        """Convert the request to a project deletion record producer

        Convert the request to a project deletion record producer. All the
        necessary fields are:

            data = {
                "username": "my_username",
                "project_name": "Openstack Project Name",
                "comment": "My important comment!!!",
            }

        :param request: a `Request` instance
        :param data: `dict` with values to create the record producer
        :returns: a `RecordProducer` instance
        """
        return self._convert_RQF(request, data, DeleteProjectRecordProducer)

    def _get_record_field_values(self, request):
        return self.client.getRecords(table_sys_id=request.info.sys_id)


class RecordProducer(object):
    """Abstract class to create record producers

    To get from SNOW which IDs matches which field to be able to
    build `LABEL_TO_ELEMENT_ID`, `LABEL_TO_ORDER` and `DECORATORS`, you
    need to create first a ticket via ServiceNow and then
    read the fields that this generated in the record producers table
    """
    __metaclass__ = abc.ABCMeta  # noqa - incompatibility with python 3.x

    # a child of this function should include these four parameters
    # that will indicate how the form is built for each specific
    # record producer
    LABEL_TO_ELEMENT_ID = None
    ELEMENT_ID_TO_LABEL = None
    LABEL_TO_ORDER = None
    DECORATORS = None

    def __init__(self, request, field_values):
        self.request = request

        for record_field in field_values:
            if record_field.question in self.element_id_to_label:
                setattr(self, self.element_id_to_label[record_field.question],
                        record_field.value)

    @abc.abstractproperty
    def element_id_to_label(self):
        raise NotImplementedError


class CreateProjectRecordProducer(RecordProducer):
    """Reference for the create project record producer"""

    LABEL_TO_ELEMENT_ID = {
        'accounting_group': 'f1450d4f4f77524015d3bc511310c7ff',
        'cores': 'a4323b70f1a389004baf946ac6346fdb',
        'description': '7c917730f1a389004baf946ac6346f4a',
        'egroup': '59d2fb70f1a389004baf946ac6346f4a',
        'instances': '8d123b70f1a389004baf946ac6346f41',
        'owner': '7233bf70f1a389004baf946ac6346f62',
        'project_name': '5f413770f1a389004baf946ac6346faf',
        'project_type': '013be4bc4f73de0015d3bc511310c7ef',
        'ram': '0e523b30f1a389004baf946ac6346f4e',
        'cp1_gigabytes': '0bcb99fe4fb6c30015d3bc511310c7d5',
        'cp1_volumes': '23ebd1fe4fb6c30015d3bc511310c7ef',
        'cp2_gigabytes': '934d1d724ff6c30015d3bc511310c7a8',
        'cp2_volumes': '956d9d724ff6c30015d3bc511310c78d',
        'cpio1_gigabytes': '667e55b24ff6c30015d3bc511310c746',
        'cpio1_volumes': '749ed9b24ff6c30015d3bc511310c768',
        'io1_gigabytes': 'ac6f5db24ff6c30015d3bc511310c7e5',
        'io1_volumes': '848f11f24ff6c30015d3bc511310c7f5',
        'standard_gigabytes': 'c0bafe85e0d3d1004baf32cd136010e5',
        'standard_volumes': '9edaf609e0d3d1004baf32cd136010d2',
        'wig_cp1_gigabytes': 'deb06df24ff6c30015d3bc511310c767',
        'wig_cp1_volumes': '1dd061364ff6c30015d3bc511310c791',
        'wig_cpio1_gigabytes': '57a125364ff6c30015d3bc511310c779',
        'wig_cpio1_volumes': '3ac1e9364ff6c30015d3bc511310c7d1',
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.iteritems()}

    LABEL_TO_ORDER = {
        'accounting_group': 7200,
        'cores': 1500,
        'description': 600,
        'egroup': 900,
        'instances': 1400,
        'owner': 800,
        'project_name': 500,
        'project_type': 7500,
        'ram': 1700,
        'cp1_gigabytes': 2900,
        'cp1_volumes': 3000,
        'cp2_gigabytes': 3500,
        'cp2_volumes': 3600,
        'cpio1_gigabytes': 4100,
        'cpio1_volumes': 4200,
        'io1_gigabytes': 4800,
        'io1_volumes': 4900,
        'standard_gigabytes': 2300,
        'standard_volumes': 2400,
        'wig_cp1_gigabytes': 5400,
        'wig_cp1_volumes': 5500,
        'wig_cpio1_gigabytes': 6000,
        'wig_cpio1_volumes': 6100,
    }

    DECORATORS = [
        ('75ffb13c115530404bafa32c34fb163e', 0),
        ('48010e3c115530404bafa32c34fb16fe', 100),
        ('b9622e634f895600e3a2119f0310c738', 200),
        ('590462a34f895600e3a2119f0310c76e', 300),
        ('fd24a6a34f895600e3a2119f0310c76a', 400),
        ('e4f276a74f895600e3a2119f0310c7e2', 700),
        ('703376274f895600e3a2119f0310c7b1', 1000),
        ('43c1f770f1a389004baf946ac6346f73', 1100),
        ('de493a09e0d3d1004baf32cd1360100f', 1200),
        ('eae17f30f1a389004baf946ac6346f75', 1300),
        ('bb92bb70f1a389004baf946ac6346fcb', 1600),
        ('b3593a09e0d3d1004baf32cd13601018', 1800),
        ('b84a7a09e0d3d1004baf32cd13601086', 1900),
        ('30faba09e0d3d1004baf32cd13601081', 2000),
        ('3497113e4fb6c30015d3bc511310c72b', 2100),
        ('acaa55be4fb6c30015d3bc511310c720', 2200),
        ('3d4bfa09e0d3d1004baf32cd1360100d', 2500),
        ('5c6bd1be4fb6c30015d3bc511310c7c8', 2600),
        ('2b8b15fe4fb6c30015d3bc511310c7a0', 2700),
        ('27ab19fe4fb6c30015d3bc511310c748', 2800),
        ('e81cd1fe4fb6c30015d3bc511310c7c8', 3100),
        ('4abcd9fe4fb6c30015d3bc511310c759', 3200),
        ('d92d55724ff6c30015d3bc511310c74a', 3400),
        ('2a7d19fe4fb6c30015d3bc511310c751', 3700),
        ('b7ed11b24ff6c30015d3bc511310c7f7', 3800),
        ('a71ed9724ff6c30015d3bc511310c796', 3900),
        ('bb3e55b24ff6c30015d3bc511310c73e', 4000),
        ('aaae55b24ff6c30015d3bc511310c7df', 4300),
        ('0ea4ae35e05bd1004baf32cd1360103c', 4400),
        ('001f59b24ff6c30015d3bc511310c7b5', 4500),
        ('932f5d724ff6c30015d3bc511310c728', 4600),
        ('f94f9db24ff6c30015d3bc511310c71d', 4700),
        ('75af5db24ff6c30015d3bc511310c709', 5000),
        ('ba4061f24ff6c30015d3bc511310c7bd', 5100),
        ('de706df24ff6c30015d3bc511310c718', 5200),
        ('7380a1f24ff6c30015d3bc511310c723', 5300),
        ('c3f021f24ff6c30015d3bc511310c740', 5600),
        ('f441e1364ff6c30015d3bc511310c7c1', 5700),
        ('136125364ff6c30015d3bc511310c74b', 5800),
        ('bb71a9f24ff6c30015d3bc511310c7d4', 5900),
        ('40e16d364ff6c30015d3bc511310c7c3', 6200),
        ('e2ab3e09e0d3d1004baf32cd13601026', 6300),
        ('db65f319e057d1004baf32cd13601032', 6400),
        ('fe46d37f0a0a8c070010a1d126c321b5', 6500),
        ('fa7942bf0a0a8c0701eccecc4ae639ae', 6600),
        ('fe4865dc0a0a8c0701a5af7d33a989d5', 6700),
        ('8099acf84f73de0015d3bc511310c7dc', 6900),
        ('b15ae0bc4f73de0015d3bc511310c7be', 7000),
        ('593db1114f0ce24015d3bc511310c7a9', 7100),
        ('a38df9114f0ce24015d3bc511310c770', 7300),
        ('345d3d9d4fc8e24015d3bc511310c70e', 7400),
        ('1cb9a07c4f73de0015d3bc511310c7ea', 7600)
    ]

    def __init__(self, request, field_values):
        super(CreateProjectRecordProducer, self).__init__(request,
                                                          field_values)

    @property
    def element_id_to_label(self):
        return CreateProjectRecordProducer.ELEMENT_ID_TO_LABEL


class QuotaUpdateRecordProducer(RecordProducer):
    """Reference for the quota update record producer"""

    LABEL_TO_ELEMENT_ID = {
        "instances": "bbd82e5078a27100de14a0934907b54f",
        "cores": "26f8ea5078a27100de14a0934907b56e",
        "ram": "3a49629078a27100de14a0934907b552",
        "project_name": "27b6a65078a27100de14a0934907b5a4",
        "cp1_gigabytes": "6c5a46682b8d42004baf9d8269da15d2",
        "cp1_volumes": "6019ca282b8d42004baf9d8269da15aa",
        "cpio1_gigabytes": "ab518c962b494200de14ff8119da1553",
        "cpio1_volumes": "81a1cc962b494200de14ff8119da1555",
        "io1_gigabytes": "e49448d62b494200de14ff8119da1554",
        "io1_volumes": "54f4c8d62b494200de14ff8119da15d9",
        "standard_gigabytes": "399aa69078a27100de14a0934907b5f0",
        "standard_volumes": "00baaa9078a27100de14a0934907b53e",
        "wig_cp1_volumes": "33024a6a4ff20600398ebc511310c794",
        "wig_cp1_gigabytes": "0c52ca6a4ff20600398ebc511310c754",
        "wig_cpio1_volumes": "6085ad034f101240398ebc511310c729",
        "wig_cpio1_gigabytes": "3297a9434f101240398ebc511310c716",
        # Extra fields to make form visualization better in SNOW
        "standard": "86ae73862b494200de14ff8119da1531",
        "cp1": "edf80e282b8d42004baf9d8269da154b",
        "cpio1": "bc2088962b494200de14ff8119da15d6",
        "io1": "73400c962b494200de14ff8119da1569",
        "wig_cp1": "09d1022a4ff20600398ebc511310c7e9",
        "wig_cpio1": "01656d034f101240398ebc511310c732",
        "visible_cp1": "abc786282b8d42004baf9d8269da1572",
        "visible_cpio1": "e11148962b494200de14ff8119da153e",
        "visible_io1": "e22280d62b494200de14ff8119da1536",
        "visible_wig_cp1": "2871466a4ff20600398ebc511310c71c",
        "visible_wig_cpio1": "4e15a5034f101240398ebc511310c759",
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.iteritems()}

    LABEL_TO_ORDER = {
        "cores": "900",
        "cp1": "2700",
        "cp1_gigabytes": "3000",
        "cp1_volumes": "2800",
        "cpio1": "4500",
        "cpio1_gigabytes": "4800",
        "cpio1_volumes": "4600",
        "instances": "800",
        "io1": "5500",
        "io1_gigabytes": "5800",
        "io1_volumes": "5600",
        "project_name": "300",
        "ram": "1100",
        "standard": "1800",
        "standard_gigabytes": "2100",
        "standard_volumes": "1900",
        "visible_cp1": "2400",
        "visible_cpio1": "4200",
        "visible_io1": "5100",
        "visible_wig_cp1": "6100",
        "visible_wig_cpio1": "7000",
        "wig_cp1": "6400",
        "wig_cp1_gigabytes": "6700",
        "wig_cp1_volumes": "6500",
        "wig_cpio1": "7300",
        "wig_cpio1_gigabytes": "7600",
        "wig_cpio1_volumes": "7400",
    }

    DECORATORS = [
        ("75ffb13c115530404bafa32c34fb163e", "0"),
        ("48010e3c115530404bafa32c34fb16fe", "100"),
        ("d3af21794f0c9e00e3a2119f0310c752", "200"),
        ("9f56bd484f585e00398ebc511310c75d", "400"),
        ("9698a25078a27100de14a0934907b5f9", "500"),
        ("5eb8265078a27100de14a0934907b5bb", "600"),
        ("9bc8ea5078a27100de14a0934907b56b", "700"),
        ("d919ae5078a27100de14a0934907b5dc", "1000"),
        ("3469629078a27100de14a0934907b5d1", "1200"),
        ("c51935f94f0c9e00e3a2119f0310c76f", "1300"),
        ("86c5710e4fd0de00398ebc511310c7a9", "1400"),
        ("a55ae29078a27100de14a0934907b5ca", "1500"),
        ("c88a2a9078a27100de14a0934907b5a5", "1600"),
        ("5e7602282b8d42004baf9d8269da1588", "1700"),
        ("f0b682282b8d42004baf9d8269da1520", "2000"),
        ("e6ca2a9078a27100de14a0934907b53a", "2200"),
        ("71ca79f94f0c9e00e3a2119f0310c794", "2300"),
        ("273ac6682b8d42004baf9d8269da1520", "2500"),
        ("a3d886282b8d42004baf9d8269da1580", "2600"),
        ("9439c2282b8d42004baf9d8269da157d", "2900"),
        ("7ba35ee82b8d42004baf9d8269da15b8", "4000"),
        ("d7b653404fd85e00398ebc511310c77c", "4100"),
        ("944108962b494200de14ff8119da15e4", "4300"),
        ("848108962b494200de14ff8119da15e6", "4400"),
        ("a7c100d62b494200de14ff8119da1587", "4700"),
        ("e60484d62b494200de14ff8119da154e", "4900"),
        ("0557df004fd85e00398ebc511310c795", "5000"),
        ("0ea4ae35e05bd1004baf32cd1360103c", "5200"),
        ("f66484d62b494200de14ff8119da1562", "5300"),
        ("2fb448d62b494200de14ff8119da15f8", "5400"),
        ("60150cd62b494200de14ff8119da1527", "5700"),
        ("472588d62b494200de14ff8119da1500", "5900"),
        ("b5a797404fd85e00398ebc511310c7a5", "6000"),
        ("bab1c26a4ff20600398ebc511310c7ec", "6200"),
        ("c2f14a6a4ff20600398ebc511310c7c3", "6300"),
        ("41724a6a4ff20600398ebc511310c7d4", "6600"),
        ("37824e6a4ff20600398ebc511310c77d", "6800"),
        ("a618df404fd85e00398ebc511310c719", "6900"),
        ("30352d034f101240398ebc511310c784", "7100"),
        ("cb452d034f101240398ebc511310c7e6", "7200"),
        ("38a52d034f101240398ebc511310c785", "7500"),
        ("b6b769434f101240398ebc511310c7e2", "7700"),
        ("71c5a58f4fdcde00398ebc511310c74d", "7800"),
        ("1fdae29078a27100de14a0934907b542", "7900"),
        ("b02b2e9078a27100de14a0934907b540", "8000"),
        ("fe46d37f0a0a8c070010a1d126c321b5", "8100"),
        ("fa7942bf0a0a8c0701eccecc4ae639ae", "8200"),
        ("fe4865dc0a0a8c0701a5af7d33a989d5", "8300"),
    ]

    def __init__(self, request, field_values):
        super(QuotaUpdateRecordProducer, self).__init__(request,
                                                        field_values)

    @property
    def element_id_to_label(self):
        return QuotaUpdateRecordProducer.ELEMENT_ID_TO_LABEL


class DeleteProjectRecordProducer(RecordProducer):
    """Reference for the delete project record producer"""

    LABEL_TO_ELEMENT_ID = {
        "project_name": "079305bf4fa2b60064cc119f0310c723",
    }

    # change keys by values and values by keys
    ELEMENT_ID_TO_LABEL = {y: x for x, y in LABEL_TO_ELEMENT_ID.iteritems()}

    LABEL_TO_ORDER = {
        "project_name": "200",
    }

    DECORATORS = [
        ("75ffb13c115530404bafa32c34fb163e", "0"),
        ("48010e3c115530404bafa32c34fb16fe", "100"),
        ("0ea4ae35e05bd1004baf32cd1360103c", "300")
    ]

    def __init__(self, request, field_values):
        super(DeleteProjectRecordProducer, self).__init__(request,
                                                          field_values)

    @property
    def element_id_to_label(self):
        return DeleteProjectRecordProducer.ELEMENT_ID_TO_LABEL
