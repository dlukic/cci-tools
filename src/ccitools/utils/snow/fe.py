from ccitools.utils import utils


class FunctionalElementSoap(object):
    FUNCTIONAL_ELEMENT_TABLE = "u_cmdb_ci_functional_services"
    RELATION_FE_SE_TABLE = "cmdb_rel_ci"

    def __init__(self, instance, transport):
        self.fe_client = utils.get_soap_client(
            instance, FunctionalElementSoap.FUNCTIONAL_ELEMENT_TABLE,
            transport
        )
        self.rel_fe_se_client = utils.get_soap_client(
            instance, FunctionalElementSoap.RELATION_FE_SE_TABLE,
            transport
        )

    def get_fe_by_name(self, name):
        """Get FE raw information from the SOAP request

        :param name: a FE name
        :returns: a zeep object to get all the information about the FE
            specified as a parameter
        """
        return self.fe_client.getRecords(name=name)[0]

    def get_best_se(self, fe_sys_id):
        """Get the best SE for the specified sys_id

        Get the best SE using the relations API of SNOW. The SNOW response
        is something like:

            [
                {
                    'child': 'cc722ecf60833880c713e946666f3fff',
                    ...
                    'u_name': 'A+: ... <-> Cloud Infrastructure',
                }, {
                    'child': 'cc722ecf60833880c713e946666f3fff',
                    ...
                    'u_name': 'C: ... <-> Cloud Infrastructure',
                }, {
                    ...
                }
            ]

        where `A+` records are SE that are the best fit for the FE, and
        `A` records are good FEs for that SE.

        :param fe_sys_id: the sys_id of the FE
        :returns: the best SE (`A+`) for the FE and if if's not possible, the
            last one we found available with `A`
        """
        BEST_SERVICE = "A+"
        GOOD_SERVICE = "A"

        rel_list = self.rel_fe_se_client.getRecords(child=fe_sys_id)

        for rel_fe_se_record in rel_list:
            if rel_fe_se_record.u_name.startswith(BEST_SERVICE):
                return rel_fe_se_record
            if rel_fe_se_record.u_name.startswith(GOOD_SERVICE):
                best_se = rel_fe_se_record
        return best_se
