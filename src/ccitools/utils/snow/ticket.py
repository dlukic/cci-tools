import abc
from ccitools.utils import utils
import json
import logging
import re


logger = logging.getLogger(__name__)


class TicketSoap(object):
    REQUESTS_TABLE = "u_request_fulfillment"
    INCIDENTS_TABLE = "incident"
    COMMENTS_TABLE = "CERNJournalFieldRead"

    def __init__(self, instance, transport):
        self.rqf_client = utils.get_soap_client(
            instance, TicketSoap.REQUESTS_TABLE, transport)
        self.inc_client = utils.get_soap_client(
            instance, TicketSoap.INCIDENTS_TABLE, transport)
        # custom special service which returns in JSON done by David Martin
        self.comments_client = utils.get_soap_client(
            instance, TicketSoap.COMMENTS_TABLE, transport)

    def create_INC(self, short_description, functional_element,
                   assignment_group, **kwargs):
        """Create a service now incident

        It will accept any parameter that can be accepted using SOAP via
        `kwargs`

        :param short_description: description for the ticket
        :param functional_element: FE to which assign the ticket
        :param assignment_group: assignment group to which assign the ticket
        :returns: an `Incident` instance
        """
        # default arguments
        if "caller_id" not in kwargs:
            kwargs["caller_id"] = ""
        if "u_business_service" not in kwargs:
            kwargs["u_business_service"] = ""
        if "u_caller_id" not in kwargs:
            kwargs["u_caller_id"] = ""

        soap_response = self.inc_client.insert(
            short_description=short_description,
            u_functional_element=functional_element,
            assignment_group=assignment_group,
            **kwargs)

        return Incident(self.inc_client, self.comments_client,
                        soap_response.number)

    def create_RQF(self, short_description, functional_element,
                   assignment_group, **kwargs):
        """Create a service now request

        It will accept any parameter that can be accepted using SOAP via
        `kwargs`

        :param short_description: description for the ticket
        :param functional_element: FE to which assign the ticket
        :param assignment_group: assignment group to which assign the ticket
        :returns: an `Request` instance
        """
        # default arguments
        if "u_business_service" not in kwargs:
            kwargs["u_business_service"] = ""
        if "u_caller_id" not in kwargs:
            kwargs["u_caller_id"] = ""

        soap_response = self.rqf_client.insert(
            short_description=short_description,
            u_functional_element=functional_element,
            assignment_group=assignment_group,
            **kwargs)

        return Request(self.rqf_client, self.comments_client,
                       soap_response.number)

    def _is_incident(self, number):
        """Check if the ticket is a incident

        :param number: a ticket number
        :returns: `True` if the ticket is an incident, `False` otherwise
        """
        return re.match("INC\d{7}", number)

    def _is_request(self, number):
        """Check if the ticket is a request

        :param number: a ticket number
        :returns: `True` if the ticket is a request, `False` otherwise
        """
        return re.match("RQF\d{7}", number)

    def get_ticket(self, ticket_number):
        """Get a ticket instance form a ticket number

        :param ticket_number: a ticket number
        :returns: a `Ticket` instance
        """
        if self._is_incident(ticket_number):
            return Incident(self.inc_client, self.comments_client,
                            ticket_number)
        elif self._is_request(ticket_number):
            return Request(self.rqf_client, self.comments_client,
                           ticket_number)

    def get_active_incidents_by_host(self, host):
        """Get a list of active incidents for the specified host

        :param host: a machine
        :returns: a list of `Incident` which are GNI active tickets for
            the specified host
        """
        query = "active=true^u_configuration_items_listLIKE{host}".format(
            host=host)

        incidents = self.inc_client.getRecords(__encoded_query=query)

        return [Incident(self.inc_client, self.comments_client, incident_info)
                for incident_info in incidents]

    def _send_encoded_query(self, query, soap_client):
        """Sends an encoded query to SNOW

        :param query: string with the full encoded query
        :parm soap_client: soap client used to send the query
        :returns: a list of matched elements from the table
        """
        return soap_client.getRecords(__encoded_query=query)

    def raw_query_requests(self, query):
        """Sends an encoded query to the rqf table

        :param query: string with the full encoded query
        :returns: a list of matched elements from the table
        """
        return self._send_encoded_query(query, self.rqf_client)

    def raw_query_incidents(self, query):
        """Sends an encoded query to the incidents table

        :param query: string with the full encoded query
        :returns: a list of matched elements from the table
        """
        return self._send_encoded_query(query, self.inc_client)

    def raw_query_comments(self, query):
        """Sends an encoded query to the comments table

        :param query: string with the full encoded query
        :returns: a list of matched elements from the table
        """
        return self._send_encoded_query(query, self.comments_client)


class TicketAbstract(object):
    """Abstract class for tickets

    All common methods for incidents and requests are implemented in this
    class. The rest
    """
    __metaclass__ = abc.ABCMeta  # noqa - python 3.x incompatibity

    def __init__(self, ticket_client, comments_client, ticket_number):
        self.client = ticket_client
        self.comments_client = comments_client
        # buffer to store saves in one to do them in one go
        self.fields_to_save = {}
        # we got here the zeep object for the ticket info
        self.info = self.client.getRecords(number=ticket_number)[0]

    def save(self):
        """Performs a request to service now to do all the changes in one go"""
        logger.info("Doing `.update` SOAP call in Service Now with: '{fields}'"
                    .format(fields=self.fields_to_save))
        self._save()  # call the child save
        self.fields_to_save = {}  # reset the state of the buffer
        # save the information of the ticket
        self.info = self.client.getRecords(number=self.info.number)[0]

    @abc.abstractmethod
    def _save(self):
        """Children method to perform changes in SNOW

        Children must implement the SOAP request for the fields
        saved in `self.fields_to_save`
        """
        raise NotImplementedError

    @abc.abstractmethod
    def resolve(self, comment, resolver):
        """Resolve the ticket

        :param comment: comment to put as the reason for resolving
        :param resolver: person who resolved the ticket
        """
        raise NotImplementedError

    @abc.abstractmethod
    def change_state(self, state):
        """Move the state of the ticket

        :param state: a `IncidentState` or a `RequestState` field
        """
        raise NotImplementedError

    def add_comment(self, comment):
        """Add a comment to the ticket

        :param comment: a comment
        """
        logger.info(u"Adding comment '{comment}' to the ticket"
                    .format(comment=utils.truncate(comment)))
        self.fields_to_save["comments"] = comment

    def _add_to_watch_list(self, email_or_user_id):
        """Add email or user id to ticket watch list

        :param email_or_user_id: an email or a user id in SNOW
        """
        if "watch_list" in self.fields_to_save:
            # If there is already some emails to be added in the
            # buffer
            new_watch_list = set(self.fields_to_save["watch_list"].split(","))
            new_watch_list.add(email_or_user_id)
        elif self.info.watch_list:
            # If the ticket has someone in the watch list already
            new_watch_list = set(self.info.watch_list.split(","))
            new_watch_list.add(email_or_user_id)
        else:
            # Otherwise, no one is in the watch list, we start a new one
            new_watch_list = set([email_or_user_id])

        self.fields_to_save["watch_list"] = ",".join(new_watch_list)

    def add_email_to_watch_list(self, email):
        """Add email to a watch list

        :param email: email to be added in the watch list
        """
        logger.info("Adding email '{email}' to the ticket".format(
                    email=email))
        self._add_to_watch_list(email)

    def add_user_id_to_watch_list(self, user_id):
        """Add user id to a watch list

        :param user_id: user id to be added in the watch list
        """
        self._add_to_watch_list(user_id)

    def add_work_note(self, work_note):
        """Add worknote to a ticket

        :param work_note: work note to be added
        """
        logger.info("Adding work note '{work_note}' to the ticket"
                    .format(work_note=utils.truncate(work_note)))
        self.fields_to_save["work_notes"] = work_note

    def change_assignee(self, username):
        """Change person assigned to the ticket

        :param username: change person assigned to the ticket
        """
        logger.info("Changing user assigned to the ticket with '{username}'"
                    .format(username=username))
        self.fields_to_save["assigned_to"] = username

    def change_fe(self, fe_sys_id, se_sys_id, assignment_group_name):
        """Change FE specifying fe_sys_ids and se_sys_ids

        :param fe_sys_id: sys id for the FE
        :param se_sys_id: sys id for the SE
        :param assignment_group_name: assignment group to assign the ticket
        """
        self.fields_to_save["u_functional_element"] = fe_sys_id
        self.fields_to_save["u_ci_relations"] = se_sys_id
        self.fields_to_save["assignment_group"] = assignment_group_name

    def update(self, **kwargs):
        """Update fields in the ticket

        To be used when no other function matches your need to update
        fields in the ticket
        """
        logger.info("Changing these parameters in the ticket: '{params}'"
                    .format(params=kwargs))
        self.fields_to_save = utils.merge_dicts(self.fields_to_save, kwargs)

    def get_comments(self):
        """Get comments from the ticket

        Get comments from the ticket with this format

            [
                {
                    "sys_created_on": "2017-08-24 14:01:19",
                    "sys_created_by": "svcrdeck",
                    "value": "My foo comment"
                }
            ]

        :returns: a list of comments
        """
        comments = self.comments_client.get(sys_id=self.info.sys_id,
                                            table_name="task",
                                            field_name="comments")
        return json.loads(comments)

    def get_work_notes(self):
        """Get work notes from the ticket

        Get work notes from the ticket with this format
            [
                {
                    "sys_created_on": "2017-08-24 14:01:19",
                    "sys_created_by": "svcrdeck",
                    "value": "My foo work note"
                }
            ]
        :returns: a list of work notes
        """
        comments = self.comments_client.get(sys_id=self.info.sys_id,
                                            table_name="task",
                                            field_name="work_notes")

        return json.loads(comments)


class RequestState(object):
    NEW = "1"
    ASSIGNED = "2"
    ACCEPTED = "3"
    IN_PROGRESS = "4"
    WAITING_FOR_USER = "5"
    WAITING_FOR_3RD_PARTY = "6"
    WAITING_FOR_PROJECT_MGMT = "7"
    WAITING_FOR_CHANGE_MGMT = "8"
    RESOLVED = "9"
    CLOSED = "10"
    ARCHIVED = "11"
    WAITING_FOR_DELIVERY = "12"
    WAITING_FOR_DETAIL_TASK = "13"
    WAITING_FOR_WORK_TASK = "15"


class Request(TicketAbstract):
    def __init__(self, client, comments_client, request_info):
        super(Request, self).__init__(client, comments_client, request_info)

    def _save(self):
        default_args = {
            "u_caller_id": self.info.u_caller_id,
            "u_functional_element": self.info.u_functional_element,
            "short_description": self.info.short_description,
            "u_business_service": self.info.u_business_service
        }

        args = utils.merge_dicts(default_args, self.fields_to_save)
        self.client.update(sys_id=self.info.sys_id, **args)

    def resolve(self, comment, resolver):
        """Resolve the request

        :param comment: comment to put as the resolved reason
        :param resolver: the user that will be the resolver of the ticket
        """
        logger.info("Resolving ticket with user '{resolver}'"
                    .format(resolver=resolver))
        logger.info("Adding comment as resolved reason: '{comment}'"
                    .format(comment=utils.truncate(comment)))

        self.fields_to_save["u_close_code"] = "Fulfilled"
        self.fields_to_save["u_workflow_stage"] = "Resolved"
        self.fields_to_save["u_current_task_state"] = RequestState.RESOLVED
        self.fields_to_save["comments"] = comment
        self.fields_to_save["assigned_to"] = resolver

    def change_state(self, state):
        """Change the state of the request

        :param state: a `RequestState` reference to the new state
        """
        self.fields_to_save["u_current_task_state"] = state


class IncidentState(object):
    NEW = "1"
    ASSIGNED = "2"
    IN_PROGRESS = "3"
    WAITING_FOR_USER = "4"
    WAITING_FOR_3RD_PARTY = "5"
    RESOLVED = "6"
    CLOSED = "7"
    WAITING_FOR_CHANGE = "8"
    WAITING_FOR_PARENT = "9"
    WORKAROUND_WAITING_FOR_3RD_PARTY = "10"
    WORKAROUND_WAITING_FOR_CHANGE = "11"
    WAITING_FOR_DETAIL_TASK = "13"


class Incident(TicketAbstract):
    def __init__(self, client, comments_client, incident_info):
        super(Incident, self).__init__(client, comments_client, incident_info)

    def _save(self):
        default_args = {
            "caller_id": self.info.caller_id,
            "u_caller_id": self.info.u_caller_id,
            "u_functional_element": self.info.u_functional_element,
            "short_description": self.info.short_description,
            "u_business_service": self.info.u_business_service
        }

        args = utils.merge_dicts(default_args, self.fields_to_save)
        self.client.update(sys_id=self.info.sys_id, **args)

    def resolve(self, comment, resolver):
        """Resolve the incident

        :param comment: comment to put as the resolved reason
        :param resolver: the user that will be the resolver of the ticket
        """
        logger.info("Resolving ticket with user '{resolver}'"
                    .format(resolver=resolver))
        logger.info("Adding comment as resolved reason: '{comment}'"
                    .format(comment=comment))

        self.fields_to_save["u_close_code"] = "Restored"
        self.fields_to_save["state"] = IncidentState.RESOLVED
        self.fields_to_save["incident_state"] = IncidentState.RESOLVED
        self.fields_to_save["assigned_to"] = resolver
        self.fields_to_save["comments"] = comment

    def change_state(self, state):
        """Change the state of the request

        :param state: a `IncidentState` reference to the new state
        """
        self.fields_to_save["incident_state"] = state
