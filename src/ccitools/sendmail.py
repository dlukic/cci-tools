from __future__ import print_function

import datetime
import logging
import smtplib
import time
import uuid

from email import Encoders  # noqa
from email.mime.multipart import MIMEMultipart  # noqa
from email.mime.text import MIMEText  # noqa
from email.MIMEBase import MIMEBase  # noqa
from email.Utils import formatdate  # noqa

logger = logging.getLogger(__name__)


class MailClient(object):

    def __init__(self, smtp_server='localhost'):
        self.smtp_server = smtp_server

    def __create_mail(self, mail_to, mail_subject, mail_body, mail_from,
                      mail_cc, mail_bcc, mail_content_type):
        msg = MIMEMultipart()
        msg['To'] = mail_to
        msg['From'] = mail_from
        msg['Cc'] = mail_cc
        msg['Bcc'] = mail_bcc
        msg['Subject'] = mail_subject
        msg.attach(MIMEText(mail_body, mail_content_type))
        return msg

    def send_mail(self, mail_to, mail_subject, mail_body, mail_from, mail_cc,
                  mail_bcc, mail_content_type):
        logger.info("Sending email to '%s'..." % (mail_to))
        msg = self.__create_mail(mail_to, mail_subject, mail_body, mail_from,
                                 mail_cc, mail_bcc, mail_content_type)
        smtpObj = smtplib.SMTP(self.smtp_server)
        smtpObj.sendmail(msg['From'], [msg['To'], msg['Cc'], msg['Bcc']],
                         msg.as_string())
        smtpObj.quit()

    def __cap(self, s, l):
        return s if len(s) <= l else s[0:l - 3] + '...'

    def set_valarm(self, trigger, description, **kwargs):
        """Quick method version that creates VALARM compontent for iCalendar

        TRIGGER and DURATION properties use following time format:
        vD --> v days
        xH --> x hours
        yM --> y minutes
        zS --> z seconds
        """
        if trigger:
            if 'D' in trigger:
                trigger = "TRIGGER;VALUE=DURATION:-P%s\n" % trigger
            else:
                trigger = "TRIGGER;VALUE=DURATION:-PT%s\n" % trigger
        else:
            raise Exception("'trigger' property must be specified.")

        repeat = ""
        duration = ""
        if 'repeat' in kwargs:
            if 'duration' in kwargs:
                repeat = 'REPEAT:%s\n' % kwargs['repeat']
                if 'D' in kwargs['duration']:
                    duration = 'DURATION:P%s\n' % kwargs['duration']
                else:
                    duration = 'DURATION:PT%s\n' % kwargs['duration']
            else:
                raise Exception("Both 'repeat' and 'duration' options must "
                                "be present when setting an alarm.")

        description = "DESCRIPTION:%s\n" % description
        alarm = ("BEGIN:VALARM\n" + trigger + repeat + duration +
                 "ACTION:DISPLAY\n" + description + "END:VALARM")
        return alarm

    def __attach_ics(self, msg, organizer, attendees, title, description, date,
                     duration, **kwargs):

        attendees = attendees.split(",")
        title = self.__cap(title, 128)

        ddtstart = datetime.datetime.fromtimestamp(
            time.mktime(time.strptime(date, "%d-%m-%Y %H:%M")))
        dur = datetime.timedelta(hours=duration)
        dtend = ddtstart + dur
        dtstamp = datetime.datetime.now().strftime("%Y%m%dT%H%M%S")
        dtstart = ddtstart.strftime("%Y%m%dT%H%M%S")
        dtend = dtend.strftime("%Y%m%dT%H%M%S")

        attendee = ""
        for att in attendees:
            attendee += "ATTENDEE;CUTYPE=INDIVIDUAL;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=" + att + ":MAILTO:" + att + "\n"  # noqa

        if 'alarm' in kwargs:
            alarm = kwargs['alarm']
        else:
            alarm = ""

        ical = """BEGIN:VCALENDAR
PRODID:-//IT-OIS-CV//RUNDECK
VERSION:2.0
METHOD:REQUEST
BEGIN:VEVENT
DTSTART:%s
DTEND:%s
DTSTAMP:%s
ORGANIZER;CN="Cloud Service Automation":mailto:%s
UID:%s
%sCREATED:%s
DESCRIPTION:%s
SEQUENCE:0
STATUS:CONFIRMED
SUMMARY:%s
TRANSP:OPAQUE
%s
END:VEVENT
END:VCALENDAR""" % (dtstart, dtend, dtstamp, organizer, uuid.uuid4(), attendee,
                    dtstamp, description, title, alarm)

        msg.set_default_type('mixed')
        msg['Reply-To'] = organizer
        msg['Date'] = formatdate(localtime=False)

        eml_body = description

        part_email = MIMEText(eml_body, "html")
        part_cal = MIMEText(ical, 'calendar;method=REQUEST')

        msgAlternative = MIMEMultipart('alternative')
        msgAlternative.attach(part_email)
        msgAlternative.attach(part_cal)

        msg.attach(msgAlternative)

        ical_atch = MIMEBase('application/ics', ' ;name="%s"' % "invite.ics")
        ical_atch.set_payload(ical)
        Encoders.encode_base64(ical_atch)
        ical_atch.add_header('Content-Disposition', 'attachment',
                             filename="%s" % ("invite.ics"))

        eml_atch = MIMEBase('text/plain', '')
        Encoders.encode_base64(eml_atch)
        eml_atch.add_header('Content-Transfer-Encoding', "")

        msgAlternative.attach(part_email)
        msgAlternative.attach(part_cal)
        return msg

    def send_mail_ics(self, mail_subject, mail_body,
                      mail_cc, mail_bcc, mail_content_type, organizer,
                      attendees, title, description, date, duration,
                      **kwargs):
        mail_to = attendees
        mail_from = organizer
        logger.info("Sending email with ICS to '%s'..." % mail_to)
        msg = self.__create_mail(mail_to, mail_subject, mail_body, mail_from,
                                 mail_cc, mail_bcc, mail_content_type)
        if 'alarm' in kwargs:
            alarm = kwargs['alarm']
            msg_ics = self.__attach_ics(msg, organizer, attendees, title,
                                        description, date, duration,
                                        alarm=alarm)
        else:
            msg_ics = self.__attach_ics(msg, organizer, attendees, title,
                                        description, date, duration)
        smtpObj = smtplib.SMTP(self.smtp_server)
        smtpObj.ehlo()
        smtpObj.sendmail(msg_ics['From'], [
                         msg_ics['To'], msg_ics['Cc'], msg_ics['Bcc']],
                         msg_ics.as_string())
        smtpObj.close()
