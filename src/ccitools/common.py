#!/usr/bin/env python

import cookielib
import logging
import os
import random
import shlex
import stat
import string
import subprocess
from uuid import UUID


def is_valid_UUID(value):
    try:
        UUID(value, version=4)
        return True
    except ValueError:
        return False


def generate_random_string(length=8):
    return ''.join(random.SystemRandom().choice(string.ascii_letters)
                   for _ in range(length))


def add_logging_arguments(parser):
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    parser.add_argument("-d", "--debug", help="increase output "
                        "to debug messages", action="store_true")


def cern_get_sso_cookie(server, cookie_file=None):
    """Get CERN SSO cookies."""

    if not cookie_file:
        cookie_file = "/tmp/cookie-" + generate_random_string()

    args = ["cern-get-sso-cookie", "--reprocess", "--url", server,
            "--outfile", cookie_file]
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    p.wait()
    os.chmod(cookie_file, stat.S_IRUSR)
    cookie = cookielib.MozillaCookieJar(cookie_file)
    cookie.load()
    os.remove(cookie_file)
    return cookie


def configure_logging(args):
    if args.verbose:
        logging.basicConfig(level=logging.INFO)
    elif args.debug:
        logging.basicConfig(level=logging.DEBUG)


def destroy_krb_ticket():
    subprocess.call(["kdestroy"])


def negociate_krb_ticket(path, username):
    kinit = '/usr/bin/kinit'
    kinit_args = [kinit, '-kt', path, username]
    kinit = subprocess.Popen(kinit_args)
    kinit.wait()


def ssh_executor(host, command, ssh_options=''):
    defaults = "-q -o StrictHostKeyChecking=no " \
               "-o UserKnownHostsFile=/dev/null " \
               "-o ConnectTimeout=10 -o ServerAliveInterval=20 " \
               "-o NumberOfPasswordPrompts=0"
    ssh_command = "ssh %s %s" % (defaults, ssh_options)
    args = shlex.split("%s root@%s %s" % (ssh_command, host, command))
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    p.wait()

    if p.returncode != 255:
        return out, err
    else:
        raise Exception("ssh: Could not access hostname %s" % host)
