#!/usr/bin/python

import inspect
import json
import logging
import re

from ccitools.errors import CciCloudNotFoundError
from cinderclient.v2 import client as cinder_client
from glanceclient.v2 import client as glance_client
from keystoneclient import exceptions
from keystoneclient.v3 import client as keystone_client
from novaclient import client as nova_client
import prettytable


# configure logging
logger = logging.getLogger(__name__)


class CloudClient(object):
    """Cloud client encapsulating the main operations in the OpenStack cloud.

    This class is designed as a facade to facilitate the interaction with
    all the components of the CERN OpenStack Cloud.
    """

    def __init__(self, session, cornerstone=None):
        """Initializes the facade to the Cloud operations.

        :param session: OS session to use in the clients
        :param auth_client: OpenstackAuthClient that does the authentication
        """
        self.keystone = keystone_client.Client(session=session)
        self.cinder = cinder_client.Client(session=session)
        self.glance = glance_client.Client(session=session)
        self.nova = nova_client.Client(version='2', session=session)
        self.cornerstone = cornerstone

    def __verify_quota_update(self, project_id, cores, instances, ram, volumes,
                              volumes_gigabytes, volume_type):
        logger.info("Verifying quotas after the update...")
        nova_quota = self.nova.quotas.get(project_id)
        cinder_quota = self.cinder.quotas.get(project_id)
        args, _, _, values = inspect.getargvalues(inspect.currentframe())
        for arg in args:
            if (arg in nova_quota._info and int(nova_quota._info[arg])
                != int(values[arg]) or
                (arg in cinder_quota._info and
                 int(cinder_quota._info[arg]) != int(values[arg]))):
                raise Exception(
                    "Project quota does NOT match requested one. "
                    "Something must have failed.")

    def add_flavor(self, project, flavor):
        self.nova.flavor_access.add_tenant_access(flavor, project)

    def has_flavor_access(self, project_name, flavor_name):
        project = self.find_project(project_name)
        flavor_list = self.nova.flavors.list(
            is_public=False) + self.inova.flavors.list(is_public=True)
        flavor = [x for x in flavor_list if x.name == flavor_name]
        if len(flavor) >= 1:
            if getattr(flavor[0], 'os-flavor-access:is_public'):
                return True

            access_list = self.nova.flavor_access.list(flavor=flavor[0])
            al = [x for x in access_list if x.tenant_id == project.id]
            return len(al) == 1

        raise CciCloudNotFoundError(
            "Could not evaluate '%s'. Flavor not found." % (flavor_name))

    def set_project_property(self, project_name, property_name,
                             property_value):
        project = self.find_project(project_name)
        r = self.cornerstone.project_properties_add(
            project.id, property_name, property_value)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception(
                "Cornerstone failed setting '%s' property, error: %s" %
                (property_name, json.loads(r.text)['property_value'][0]))

    def add_group_member(self, project, member_name):
        project_members = self.get_project_members(project.name)
        if member_name.lower() in (name.lower() for name in project_members):
            logger.info("Egroup '%s' already a member of '%s'" %
                        (member_name, project.name))
            return

        logger.info("Adding group '%s' as member of '%s'..." %
                    (member_name, project.name))
        r = self.cornerstone.add_project_members(
            project.id, "Member", member_name)
        status_code = str(r.status_code)
        if not status_code.startswith('2'):
            raise Exception(
                "Cornerstone failed adding egroup '%s', error: %s" %
                (member_name, json.loads(r.text)['property_value'][0]))

        logger.info("'%s'has been added to '%s'" % (member_name, project.name))

    def lock_servers(self, servers):
        """Locks servers of a given list.

        :param servers: List of servers to be locked.
        """
        for server in servers:
            logger.info("Locking server '%s' ..." % server.name)
            self.nova.servers.lock(server)

    def unlock_servers(self, servers):
        """Unlocks servers of a given list.

        :param servers: List of servers to be unlocked.
        """
        for server in servers:
            logger.info("Unlocking server '%s' ..." % server.name)
            self.nova.servers.unlock(server)

    def power_on_servers(self, servers):
        """Power on servers of a given list.

        :param servers: List of servers to be started.
        """
        for server in servers:
            if getattr(server, 'OS-EXT-STS:vm_state') != 'active':
                logger.info("Power on server '%s' ..." % server.name)
                self.nova.servers.start(server)
            else:
                logger.debug("Server '%s' is already active." % server.name)

    def power_off_servers(self, servers):
        """Power off servers of a given list.

        :param servers: List of servers to be stopped.
        """
        for server in servers:
            if getattr(server, 'OS-EXT-STS:vm_state') != 'stopped':
                logger.info("Power off server '%s' ..." % server.name)
                self.nova.servers.stop(server)
            else:
                logger.debug("Server '%s' is already stopped." % server.name)

    def find_project(self, reference):
        """Finds a project by name or ID

        :param reference: either project name or ID
        """
        logger.info("Looking for project '%s'..." % (reference))
        try:
            return self.keystone.projects.find(name=reference)
        except exceptions.NotFound:
            return self.keystone.projects.get(reference)

    def get_hypervisor_by_name(self, hypervisor_name):
        """Returns the hypervisor object

        :param hypervisor_name
        """
        if re.search("(.*).cern.ch", hypervisor_name):
            hypervisor_name = re.search(
                "(.*).cern.ch", hypervisor_name).group(1)
        hvs = self.nova.hypervisors.search(hypervisor_name, True)
        # return exact match (hypervisor_hostname may or may not
        # contain cern.ch)
        return [hv for hv in hvs
                if hv.hypervisor_hostname.split(".cern.ch")[0].lower()
                == hypervisor_name.lower()][0]

    def get_server(self, server_name):
        """Search the specified server by name

        :param server_name: Name of the server to search
        :returns: Server object
        """
        if re.search("(.*).cern.ch", server_name):
            server_name = re.search(
                "(.*).cern.ch", server_name).group(1)
        pattern = '^' + server_name
        servers = self.nova.servers.list(search_opts={'all_tenants': True,
                                                      'name': pattern})
        # following lines solve the unlikely case where multiple VMs
        # begin with the provided server name
        for server in servers:
            if server.name == server_name:
                return server

    def get_servers_by_hypervisor(self, hypervisor_name):
        """List all the servers running on the specified Hypervisor

        :param hypervisor_name
        """
        # we remove the .cern.ch part, because we need to check if it
        # goes with or without .cern.ch
        hypervisor = hypervisor_name.replace(".cern.ch", "")

        servers = (
            self.nova.servers.list(
                search_opts={
                    "host": hypervisor,
                    "all_tenants": 1
                })
            +
            self.nova.servers.list(
                search_opts={
                    "host": hypervisor + ".cern.ch",
                    "all_tenants": 1
                })
        )

        return [self.nova.servers.get(server.id) for server in servers]

    def get_servers_by_project(self, project_name):
        """List all the servers running on the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        return self.nova.servers.list(search_opts={'all_tenants': True,
                                                   'project_id': project.id})

    def get_servers_by_last_action_user(self, servers, username):
        """Searches for servers that were whose last action
        was performed by the specified user

        :param servers: List of target servers
        :param username: User that performed the operation
        :returns: List of matched servers
        """
        matched_servers = []
        for server in servers:
            actions = self.nova.instance_action.list(server)
            if actions:
                if getattr(actions[0], 'user_id') == username:
                    matched_servers.append(server)

        return matched_servers

    def get_servers_by_vm_state(self, servers, vm_state):
        """Searches for servers with a particular specified vm_state
        from a given list

        :param servers: List of servers
        :param vm_state: VM state of the servers
        :returns: List of matched servers
        """
        matched_servers = []
        for server in servers:
            if getattr(server, 'OS-EXT-STS:vm_state') == vm_state:
                matched_servers.append(server)

        return matched_servers

    def get_project_owner(self, project_name):
        """Returns a the owner of a project

        :param project_name
        """
        logger.debug("Trying to get owner of '%s'..." % (project_name))
        project = self.find_project(project_name)
        project_role_assignments = self.keystone.role_assignments.list(
            project=project, include_names=True)
        for project_role_assignment in project_role_assignments:
            if project_role_assignment.role['name'] == 'owner':
                return project_role_assignment.user['id']

    def get_project_members(self, project_name):
        """Returns a list of all the members of a project

        :param project_name
        """
        members = []
        logger.debug("Trying to get members of '%s'..." % (project_name))
        project = self.find_project(project_name)
        project_role_assignments = self.keystone.role_assignments.list(
            project=project)
        for project_role_assignment in project_role_assignments:
            if (hasattr(project_role_assignment, 'user')
                    and project_role_assignment.user['id'] not in members):

                members.append(project_role_assignment.user['id'])
                logger.debug("Found user '%s' as member of project '%s'" % (
                    project_role_assignment.user['id'], project.name))

            if (hasattr(project_role_assignment, 'group') and
                    project_role_assignment.group['id'] not in members):

                members.append(project_role_assignment.group['id'])
                logger.debug("Found egroup '%s' as member of project '%s'" % (
                    project_role_assignment.group['id'], project.name))
        return members

    def get_project_images(self, project_name):
        """List all the images saved on the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        images_list = []
        for image in self.glance.images.list():
            if image['owner'] == project.id:
                images_list.append(image)
        return images_list

    def get_project_instance_snapshots(self, project_name):
        """List all the instance images saved on the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        return self.glance.images.list(filters={"owner": project.id,
                                                'visibility': 'private'})

    def get_project_volumes(self, project_name):
        """List all the volumes attached to the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        return self.cinder.volumes.list(search_opts={'all_tenants': True,
                                                     'project_id': project.id})

    def get_project_volume_snapshots(self, project_name):
        """List all the volume snapshots saved on the specified tenant

        :param project_name
        """
        project = self.find_project(project_name)
        return self.cinder.volume_snapshots.list(
            search_opts={'all_tenants': True, 'project_id': project.id})

    def get_nova_service(self, hypervisor_name):
        """Returns the service running on the specified hypervisor

        :param hypervisor_name
        """
        hypervisor = self.get_hypervisor_by_name(hypervisor_name)
        host = "%s@%s" % (
            re.search('(.*)@(.*)', hypervisor.id).group(1),
            hypervisor.hypervisor_hostname)
        return self.nova.services.list(host=host)[0]

    def get_printable_quota(self, project_name):
        """Returns the quota for a given project (Cinder and Nova)

        :param project_name
        """
        t = prettytable.PrettyTable(["Service", "Quota", "Value"])
        project = self.find_project(project_name)
        nova_quota = self.nova.quotas.get(project.id)
        logger.info(nova_quota.__dict__)
        t.add_row(["Nova", "Cores", "%s" % (nova_quota.cores)])
        t.add_row(["Nova", "Instances", "%s" % (nova_quota.instances)])
        t.add_row(["Nova", "RAM", "%s GB" % (int(nova_quota.ram) / 1024)])

        cinder_quota = self.cinder.quotas.get(project.id)
        logger.info(cinder_quota.__dict__)
        t.add_row(["Cinder", "Diskspace on volumes", "%s GB" %
                   (cinder_quota.gigabytes)])
        t.add_row(["Cinder", "Volumes", "%s" % (cinder_quota.volumes)])

        t.border = True
        t.header = True
        t.align["Field"] = 'r'
        t.align["Value"] = 'l'

        return t

    def get_region_name(self, hypervisor_name):
        """Returns the region of a hypervisor

        :param hypervisor_name
        """
        hypervisor = self.get_hypervisor_by_name(hypervisor_name)
        return re.search('father!(.*)@', hypervisor.id).group(1)

    def get_user_servers(self, username, project_name=None):
        """List all servers of the specified user and project (optional)

        :param username: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        """
        if project_name:
            project = self.find_project(project_name)
            return self.nova.servers.list(
                        search_opts={'all_tenants': True,
                                     'user_id': username,
                                     'project_id': project.id})
        else:
            return self.nova.servers.list(search_opts={'all_tenants': True,
                                                       'user_id': username})

    def get_user_volumes(self, username, project_name=None):
        """List all volumes of the specified user and project (optional)

        :param username: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        """
        if project_name:
            project = self.find_project(project_name)
            return self.cinder.volumes.list(
                        search_opts={'all_tenants': True,
                                     'user_id': username,
                                     'project_id': project.id})
        else:
            return self.cinder.volumes.list(search_opts={'all_tenants': True,
                                                         'user_id': username})

    def get_printable_user_volumes(self, username, project_name=None):
        """Returns printable list of volumes from a given user

        :param username: Name of OpenStack user
        :param project_name: OpenStack project where to search in
        :returns: Table with detailed information
        """
        if project_name:
            t = prettytable.PrettyTable(["ID", "Created",
                                         "Name", "Status", "Host"])
            project = self.find_project(project_name)
            volumes = self.cinder.volumes.list(
                            search_opts={'all_tenants': True,
                                         'user_id': username,
                                         'project_id': project.id})
            for volume in volumes:
                host = volume.__dict__['os-vol-host-attr:host']
                t.add_row([volume.id, volume.created_at, volume.name,
                           volume.status, host])

        else:
            t = prettytable.PrettyTable(["Project", "ID", "Created",
                                         "Name", "Status", "Host"])
            volumes = self.get_user_volumes(username)
            for volume in volumes:
                project = self.find_project(
                        volume.__dict__['os-vol-tenant-attr:tenant_id'])
                host = volume.__dict__['os-vol-host-attr:host']
                t.add_row([project.name, volume.id, volume.created_at,
                           volume.name, volume.status, host])

        t.border = True
        t.header = True
        t.align = 'l'
        return t

    def update_quota(self, project_id, cores, instances, ram, volumes,
                     volumes_gigabytes, volume_type):
        """Updates the quota in Cinder and Nova for a given project.

        :project_id: the project id that will be updated.
        :cores: number of cores to be set in Nova.
        :instances: number of instances to be set in Nova.
        :ram: amount of ram to be set in Nova.
        :volumes: number of volumes in Cinder.
        :volumes_gigabytes: amount of gigabytes available in Cinder.
        """
        logger.info("Updating quota on project '%s'" % (project_id))
        self.update_quota_nova(project_id, cores=cores,
                               instances=instances, ram=ram)
        self.update_quota_cinder(
            project_id, volumes, volumes_gigabytes, volume_type)
        self.__verify_quota_update(
            project_id, cores, instances, ram, volumes, volumes_gigabytes,
            volume_type)
        logger.info("Cinder and Nova quota updated successfully")

    def update_quota_global_cinder(self, project_id):
        updates = {}
        cinder_quota = self.cinder.quotas.get(project_id)

        global_volumes = 0
        global_gigabytes = 0

        for key in cinder_quota._info:
            if cinder_quota._info[key] != -1:
                if 'volumes_' in key:
                    global_volumes += int(cinder_quota._info[key])
                if 'gigabytes_' in key:
                    global_gigabytes += int(cinder_quota._info[key])

        # the * 2 is to give space to people who backup their machines every
        # day (this space is to allow them to create a new snapshot without
        # deleting the one from the previous day
        global_snapshots = global_volumes * 2

        updates['volumes'] = global_volumes
        updates['gigabytes'] = global_gigabytes
        updates['snapshots'] = global_snapshots

        self.cinder.quotas.update(project_id, **updates)
        logger.info("Cinder global updated, volumes: %s gigabytes: %s "
                    "snapshots: %s" %
                    (global_volumes, global_gigabytes, global_snapshots))

    def update_quota_cinder(self, project_id, volumes, gigabytes, snapshots,
                            volume_type):
        """Updates Cinder quota for a specific volume type

        Only updates if the specified value and the current are different

        :project_id: the project id that will be updated.
        :volumes: number of volumes.
        :gigabytes: amount of gigabytes available
        :volume_type: Volume Type
        """
        updates = {}
        if volumes is not None:
            updates['volumes_%s' % volume_type] = volumes
        if gigabytes is not None:
            updates['gigabytes_%s' % volume_type] = gigabytes
        if snapshots is not None:
            updates['snapshots_%s' % volume_type] = snapshots

        self.cinder.quotas.update(project_id, **updates)
        logger.info("Cinder quota updated: %s" % updates)
        self.update_quota_global_cinder(project_id)
        logger.info("Cinder global quota updated.")

    def update_quota_nova(self, project_id, cores, instances, ram):
        """Updates Nova quota

        :project_id: The project id that will be updated
        :cores: Number of cores
        :instances: Number of instances
        :ram: Amount of RAM memory
        """
        updates = {}
        if cores is not None:
            updates["cores"] = cores
        if instances is not None:
            updates["instances"] = instances
        if ram is not None:
            updates["ram"] = ram

        self.nova.quotas.update(project_id, **updates)
        logger.info("Nova quota updated: %s" % updates)
