"""
Test integration with a real instance of ServiceNow (cerntest)
It relies in having a kerberos token
"""
from ccitools.servicenowv2 import ServiceNowClient
from ccitools.utils.snow import ticket
import pytest

# Variables to be used
snowclient = ServiceNowClient(instance="cerntest")

BEST_SE_FOR_HW_ID = "f060ebc7f0d79d8079046a0c9f0e015c"
HW_FE_NAME = "HW Resources"
HW_FE_ID = "e0c374e2a164e0009878f17945ca4f85"
HW_2ND_LINE_AG_NAME = "HW Resources 2nd Line Support"
HW_2ND_LINE_AG_ID = "c1d4b826a164e0009878f17945ca4f52"

BEST_SE_FOR_CLOUD_ID = "c5832e8360c33880c713e946666f3f98"
CLOUD_FE_NAME = "Cloud Infrastructure"
CLOUD_FE_ID = "cc722ecf60833880c713e946666f3fff"
CLOUD_3RD_LINE_AG_ID = "33c22e0360c33880c713e946666f3f61"
CLOUD_3RD_LINE_AG_NAME = "Cloud Infrastructure 3rd Line Support"

# Helper variables
REQUEST = "request"
INCIDENT = "incident"

real_request = {
   "ticket_type": REQUEST,
   "short_description": "foo",
   "functional_element": HW_FE_NAME,
   "assignment_group": HW_2ND_LINE_AG_NAME,
}

real_incident = {
   "ticket_type": INCIDENT,
   "short_description": "bar",
   "functional_element": CLOUD_FE_NAME,
   "assignment_group": CLOUD_3RD_LINE_AG_NAME,
}

@pytest.mark.parametrize(
    "short_description,functional_element,assignment_group,expected_fe_id,"
    "expected_ag_id",
    (
        (
            "foo",
            HW_FE_NAME,
            HW_2ND_LINE_AG_NAME,
            HW_FE_ID,
            HW_2ND_LINE_AG_ID,
        ),
        (
            "bar",
            CLOUD_FE_NAME,
            CLOUD_3RD_LINE_AG_NAME,
            CLOUD_FE_ID,
            CLOUD_3RD_LINE_AG_ID,
        ),
    )
)
def test_create_incident_and_get_information(short_description,
                                             functional_element,
                                             assignment_group,
                                             expected_fe_id,
                                             expected_ag_id):
    incident = snowclient.ticket.create_INC(
        short_description=short_description,
        functional_element=functional_element,
        assignment_group=assignment_group
    )

    incident = snowclient.ticket.get_ticket(incident.info.number)

    assert incident.info.short_description == short_description
    assert incident.info.u_functional_element == expected_fe_id
    assert incident.info.assignment_group == expected_ag_id


@pytest.mark.parametrize(
    "short_description,functional_element,assignment_group,expected_fe_id,"
    "expected_ag_id",
    (
        (
            "foo",
            HW_FE_NAME,
            HW_2ND_LINE_AG_NAME,
            HW_FE_ID,
            HW_2ND_LINE_AG_ID,
        ),
        (
            "bar",
            CLOUD_FE_NAME,
            CLOUD_3RD_LINE_AG_NAME,
            CLOUD_FE_ID,
            CLOUD_3RD_LINE_AG_ID,
        ),
    )
)
def test_create_request_and_get_information(short_description,
                                            functional_element,
                                            assignment_group,
                                            expected_fe_id,
                                            expected_ag_id):
    request = snowclient.ticket.create_RQF(
        short_description=short_description,
        functional_element=functional_element,
        assignment_group=assignment_group
    )

    request = snowclient.ticket.get_ticket(request.info.number)

    assert request.info.short_description == short_description
    assert request.info.u_functional_element == expected_fe_id
    assert request.info.assignment_group == expected_ag_id


def _create_ticket(ticket_type, short_description, functional_element,
                   assignment_group):
    """Create INC or RQF depending on the type

    This function is just a helper to test the different
    snowclient methods

    :param ticket_type: REQUEST or INCIDENT, depending on which kind of ticket
        we want to create
    :returns: The object returned by SNOW via SOAP
    :raises: Exception, if the type is not REQUEST or INCIDENT
    """
    if ticket_type == INCIDENT:
        ticket = snowclient.ticket.create_INC(
            short_description=short_description,
            functional_element=functional_element,
            assignment_group=assignment_group)
    elif ticket_type == REQUEST:
        ticket = snowclient.ticket.create_RQF(
            short_description=short_description,
            functional_element=functional_element,
            assignment_group=assignment_group)
    else:
        raise Exception("Please specify either INCIDENT or REQUEST as ticket "
                        "type")

    return ticket

@pytest.mark.parametrize(
    "ticket,comment",
    (
        (
            real_incident,
            "My foo comment"
        ),
        (
            real_request,
            "My bar comment"
        ),
    )
)
def test_create_ticket_and_add_comment(ticket, comment):
    my_ticket = _create_ticket(**ticket)
    my_ticket.add_comment(comment)
    my_ticket.save()

    comments = my_ticket.get_comments()

    assert comments[0]["value"] == comment


@pytest.mark.parametrize(
    "ticket,email",
    (
        (
            real_incident,
            "cloud-3rd-level-infrastructure@cern.ch"
        ),
        (
            real_request,
            "another.different.email@cern.ch"
        ),
    )
)
def test_create_ticket_and_add_email_watch_list(ticket, email):
    my_ticket = _create_ticket(**ticket)
    my_ticket.add_email_to_watch_list(email)
    my_ticket.save()

    assert my_ticket.info.watch_list == email

@pytest.mark.parametrize(
    "ticket,user,user_id",
    (
        (
            real_incident,
            "vaneldik",
            "ea525c5d0a0a8c0a00a7a9680f91774a"
        ),
        (
            real_request,
            "wiebalck",
            "ea543ec30a0a8c0a0182151c1c325cae"
        ),
    )
)
def test_create_ticket_and_add_user_watch_list(ticket, user, user_id):
    my_ticket = _create_ticket(**ticket)
    snowclient.add_user_watch_list(my_ticket, user)
    my_ticket.save()

    assert my_ticket.info.watch_list == user_id

@pytest.mark.parametrize(
    "ticket",
    (
        real_incident,
        real_request,
    )
)
def test_create_ticket_and_add_user_and_email_to_watch_list(ticket):
    user = "vaneldik"
    user_id = "ea525c5d0a0a8c0a00a7a9680f91774a"
    email = "cloud-infrastructure-3rd-level@cern.ch"

    my_ticket = _create_ticket(**ticket)
    snowclient.add_user_watch_list(my_ticket, user)
    my_ticket.add_email_to_watch_list(email)
    my_ticket.save()

    assert my_ticket.info.watch_list == "{user_id},{email}".format(
            user_id=user_id, email=email)

@pytest.mark.parametrize(
    "ticket,work_note",
    (
        (
            real_incident,
            "My foo work note"
        ),
        (
            real_request,
            "My bar work note"
        ),
    )
)
def test_create_ticket_and_add_work_note(ticket, work_note):
    my_ticket = _create_ticket(**ticket)
    my_ticket.add_work_note(work_note)
    my_ticket.save()

    comments = my_ticket.get_work_notes()
    assert comments[0]["value"] == work_note


@pytest.mark.parametrize(
    "ticket,user,user_id",
    (
        (
            real_incident,
            "Jan Van Eldik",
            "ea525c5d0a0a8c0a00a7a9680f91774a"
        ),
        (
            real_request,
            "Arne Wiebalck",
            "ea543ec30a0a8c0a0182151c1c325cae"
        ),
    )
)
def test_create_ticket_and_change_assignee(ticket, user, user_id):
    my_ticket = _create_ticket(**ticket)
    my_ticket.change_assignee(user)
    my_ticket.save()

    assert my_ticket.info.assigned_to == user_id

@pytest.mark.parametrize(
    "ticket,new_fe,new_ag,new_fe_id,new_ag_id",
    (
        (
            real_incident,
            CLOUD_FE_NAME,
            CLOUD_3RD_LINE_AG_NAME,
            CLOUD_FE_ID,
            CLOUD_3RD_LINE_AG_ID
        ),
        (
            real_request,
            HW_FE_NAME,
            HW_2ND_LINE_AG_NAME,
            HW_FE_ID,
            HW_2ND_LINE_AG_ID
        ),
    )
)
def test_create_ticket_and_change_functional_element(ticket, new_fe, new_ag,
                                                     new_fe_id, new_ag_id):
    my_ticket = _create_ticket(**ticket)
    snowclient.change_functional_element(my_ticket, new_fe, new_ag)
    my_ticket.save()

    assert my_ticket.info.u_functional_element == new_fe_id
    assert my_ticket.info.assignment_group == new_ag_id


@pytest.mark.parametrize(
    "ticket,new_state,state_field,expected_state",
    (
        (
            real_incident,
            ticket.IncidentState.WAITING_FOR_USER,
            "incident_state",
            "4"
        ),
        (
            real_request,
            ticket.RequestState.WAITING_FOR_3RD_PARTY,
            "u_current_task_state",
            "6"
        ),
    )
)
def test_create_ticket_and_change_state(ticket, new_state, state_field,
                                               expected_state):
    my_ticket = _create_ticket(**ticket)
    my_ticket.change_state(new_state)
    my_ticket.save()

    assert str(getattr(my_ticket.info, state_field)) == expected_state


@pytest.mark.parametrize(
    "ticket,comment,resolver,state_field,expected_assigned,"
    "expected_state",
    (
        (
            real_incident,
            "My foo comment",
            "wiebalck",
            "incident_state",
            "ea543ec30a0a8c0a0182151c1c325cae",
            6

        ),
        (
            real_request,
            "My bar comment",
            "vaneldik",
            "u_current_task_state",
            "ea525c5d0a0a8c0a00a7a9680f91774a",
            "9"
        ),
    )
)
def test_create_ticket_and_resolve(ticket, comment, resolver,
                                          state_field, expected_assigned,
                                          expected_state):
    my_ticket = _create_ticket(**ticket)
    my_ticket.resolve(comment, resolver)
    my_ticket.save()

    assert my_ticket.info.assigned_to == expected_assigned
    assert getattr(my_ticket.info, state_field) == expected_state

    comments = my_ticket.get_comments()
    assert comments[0]["value"] == comment


def test_create_project_creation():
    data_creation = {
        "accounting_group": "ALICE",
        "comment": "Hello, testing",
        "cores": 5,
        "description": "my description",
        "egroup": "cloud-infrastructure-3rd-level",
        "instances": 5,
        "owner": "lpigueir",
        "project_name": "Openstack test",
        "ram": 10,
        "username": "lpigueir",
        "cp1_gigabytes": 201,
        "cp1_volumes": 202,
        "cpio1_gigabytes": 203,
        "cpio1_volumes": 204,
        "io1_gigabytes": 205,
        "io1_volumes": 206,
        "standard_gigabytes": 207,
        "standard_volumes": 208,
        "wig_cp1_gigabytes": 209,
        "wig_cp1_volumes": 210,
        "wig_cpio1_gigabytes": 211,
        "wig_cpio1_volumes": 212,
    }

    expected_result = {
        "accounting_group": "ALICE",
        "cores": "5",
        "description": "my description",
        "egroup": "cloud-infrastructure-3rd-level",
        "instances": "5",
        "owner": "lpigueir",
        "project_name": "Openstack test",
        "ram": "10",
        "cp1_gigabytes": "201",
        "cp1_volumes": "202",
        "cpio1_gigabytes": "203",
        "cpio1_volumes": "204",
        "io1_gigabytes": "205",
        "io1_volumes": "206",
        "standard_gigabytes": "207",
        "standard_volumes": "208",
        "wig_cp1_gigabytes": "209",
        "wig_cp1_volumes": "210",
        "wig_cpio1_gigabytes": "211",
        "wig_cpio1_volumes": "212",
    }

    request = snowclient.ticket.create_RQF(
        short_description="My test request",
        functional_element=HW_FE_NAME,
        assignment_group=HW_2ND_LINE_AG_NAME
    )

    record_producer = snowclient.record_producer.convert_RQF_to_project_creation(
        request, data_creation)
    record_producer.request.save()

    # simulate a get
    record_producer = snowclient.get_project_creation_rp(request.info.number)
    print record_producer.request.info.number

    comment = record_producer.request.get_comments()
    assert comment[0]["value"] == ("Hello, testing\n\n"
                                   "Cloud Service Automation "
                                   "(on behalf of the user)")

    for key, value in expected_result.iteritems():
        assert getattr(record_producer, key) == value

def test_create_project_deletion():
    data_deletion = {
        "username": "lpigueir",
        "comment": "Hello, testing",
        "project_name": "Whatever"
    }

    expected_result = {
        "project_name": "Whatever"
    }

    request = snowclient.ticket.create_RQF(
        short_description="My test request",
        functional_element=CLOUD_FE_NAME,
        assignment_group=CLOUD_3RD_LINE_AG_NAME
    )

    record_producer = snowclient.record_producer.convert_RQF_to_project_deletion(  # noqa
        request, data_deletion)
    record_producer.request.save()

    # simulate a get
    record_producer = snowclient.get_project_deletion_rp(request.info.number)

    comment = record_producer.request.get_comments()

    assert comment[0]["value"] == ("Hello, testing\n\n"
                                   "Cloud Service Automation "
                                   "(on behalf of the user)")

    for key, value in expected_result.iteritems():
        assert getattr(record_producer, key) == value


def test_quota_update():
    data_quota_update = {
        "username": "lpigueir",
        "project_name": "Cloud Monitoring",
        "comment": "Hello, testing",
        "instances": "80",
        "cores": "213",
        "ram": "406",
        "cp1_gigabytes": "1",
        "cp1_volumes": "2",
        "cpio1_gigabytes": "3",
        "cpio1_volumes": "4",
        "io1_gigabytes": "5",
        "io1_volumes": "6",
        "standard_gigabytes": "7",
        "standard_volumes": "8",
        "wig_cp1_volumes": "9",
        "wig_cp1_gigabytes": "10",
        "wig_cpio1_volumes": "11",
        "wig_cpio1_gigabytes": "12",
    }

    expected_result = {
        "project_name":"Cloud Monitoring",
        "instances": "80",
        "cores": "213",
        "ram": "406",
        "cp1_gigabytes": "1",
        "cp1_volumes": "2",
        "cpio1_gigabytes": "3",
        "cpio1_volumes": "4",
        "io1_gigabytes": "5",
        "io1_volumes": "6",
        "standard_gigabytes": "7",
        "standard_volumes": "8",
        "wig_cp1_volumes": "9",
        "wig_cp1_gigabytes": "10",
        "wig_cpio1_volumes": "11",
        "wig_cpio1_gigabytes": "12",
    }

    request = snowclient.ticket.create_RQF(
        short_description="My test request",
        functional_element=HW_FE_NAME,
        assignment_group=CLOUD_3RD_LINE_AG_NAME
    )

    record_producer = snowclient.record_producer.convert_RQF_to_quota_update(
        request, data_quota_update)
    record_producer.request.save()

    # Simulate a get
    record_producer = snowclient.get_quota_update_rp(request.info.number)

    comment = record_producer.request.get_comments()

    assert comment[0]["value"] == ("Hello, testing\n\n"
                                   "Cloud Service Automation "
                                   "(on behalf of the user)")

    for key, value in expected_result.iteritems():
        assert getattr(record_producer, key) == value
