import unittest

from ccitools.xldap import XldapClient
from ccitools.errors import CciXldapNotFoundError


class TestXldap(unittest.TestCase):

    def setUp(self):
        #self.xldap = XldapClient('ldap://xldap.cern.ch')
        self.xldap = XldapClient('ldap://cerndc.cern.ch', kerberized=True)

    def test_is_existing_user_false(self):
        self.assertFalse(self.xldap.is_existing_user(''))
        self.assertFalse(self.xldap.is_existing_user(
            'madeonthespotcernusername'))
        self.assertFalse(self.xldap.is_existing_user(
            'cloud-infrastructure-developers'))
        self.assertFalse(self.xldap.is_existing_user('cci-rundeck-01.cern.ch'))

    def test_is_existing_user_true(self):
        self.assertTrue(self.xldap.is_existing_user('danielfr'))
        self.assertTrue(self.xldap.is_existing_user('svcrdeck'))

    def test_is_existing_egroup_false(self):
        self.assertFalse(self.xldap.is_existing_egroup(''))
        self.assertFalse(self.xldap.is_existing_egroup(
            'madeonthespotcernegroupname'))
        self.assertFalse(self.xldap.is_existing_egroup('danielfr'))
        self.assertFalse(self.xldap.is_existing_egroup(
            'cci-rundeck-01.cern.ch'))

    def test_is_existing_egroup_true(self):
        self.assertTrue(self.xldap.is_existing_egroup(
            'cloud-infrastructure-developers'))

    def test_get_vm_main_user(self):
        self.assertEqual(self.xldap.get_vm_main_user(
            "w7dfr.cern.ch"), "danielfr")
        self.assertEqual(self.xldap.get_vm_main_user("w7dfr"), "danielfr")
        self.assertEqual(self.xldap.get_vm_main_user(
            "cci-rundeck-01"), "cloud-infrastructure-workflows-admin")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_main_user("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_main_user("madeonthespotcernvmname")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_main_user(None)

    def test_get_vm_owner(self):
        self.assertEqual(self.xldap.get_vm_owner("w7dfr.cern.ch"), "danielfr")
        self.assertEqual(self.xldap.get_vm_owner("w7dfr"), "danielfr")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_owner("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_owner("madeonthespotcernvmname")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_vm_owner(None)

    def test_get_egroup_email(self):
        self.assertEqual(self.xldap.get_egroup_email("cloud-infrastructure-developers"),
                         "cloud-infrastructure-developers@cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_email("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_email("madeonthespotcernegroupname")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_email(None)

    def test_get_egroup_members(self):
        self.assertEqual(self.xldap.get_egroup_members("cloud-infrastructure-workflows-user"),
                         ['benoel', 'mferminl'])
        self.assertEqual(self.xldap.get_egroup_members("brains-admins"),
                         [])
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_members("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_members("danielfr")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_members("cci-rundeck-01.cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_egroup_members(None)

    def test_get_user_email(self):
        self.assertEqual(self.xldap.get_user_email(
            "danielfr"), "daniel.fernandez@cern.ch")
        self.assertEqual(self.xldap.get_user_email(
            "svcrdeck"), "svc.rdeck@cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_user_email("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_user_email("cci-rundeck-01.cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_user_email("cloud-infrastructure-developers")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_user_email(None)

    def test_get_email(self):
        self.assertEqual(self.xldap.get_email(
            "danielfr"), "daniel.fernandez@cern.ch")
        self.assertEqual(self.xldap.get_email("svcrdeck"), "svc.rdeck@cern.ch")
        self.assertEqual(self.xldap.get_email("cloud-infrastructure-developers"),
                         "cloud-infrastructure-developers@cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_email("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_email("cci-rundeck-01.cern.ch")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_email(None)

    def test_get_security_groups_members(self):
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_security_groups_members("")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_security_groups_members("whatever")
        with(self.assertRaises(CciXldapNotFoundError)):
            self.xldap.get_security_groups_members(None)


if __name__ == '__main__':
    unittest.main()
